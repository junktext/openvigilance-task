<?php
/* --------------------------------------------------------------------------------------------------------------
 * Copyright (C) 2018 by William Paul Liggett (junktext@junktext.com)
 * This Source Code Form is subject to the terms of the Mozilla Public License (MPL), v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Filename:  settings.php
 * 
 * Purpose:   General settings that affect the functionality of the OpenVigilance Task application.
 * 
 * Details:   This file should be referenced via a `require_once' for each main page, not for every sub-page
 *            (such as sub-pages that are meant to be used via an `include' or `require' themselves).
 * -------------------------------------------------------------------------------------------------------------- */

// --------------------------------
// Protected Site Config Directory: 
// --------------------------------
// These are meant to be placed ABOVE the main WWW-accesible directory so that an attacker from the Internet
// could not easily inject a capability to browse the contents of the files if a vulnerability (system or app) was
// exploitable. For example, if the web server had all of its Internet-facing content in the "public_html" folder
// and the main OpenVigilance Task directory is located at: /home/web_server/public_html/openvigilance-task
//
// Then, it would be better to place the protected site config directory here: /home/web_server/protected_site_configs
// This way, an outside attacker couldn't simply do: www.yoursite.com/../protected_site_configs (the ".." wouldn't work!)
define("PROTECTED_SITE_CONFIGS_DIR", "../../../protected_site_configs/");
