<?php
/* --------------------------------------------------------------------------------------------------------------
 * Copyright (C) 2018 by William Paul Liggett (junktext@junktext.com)
 * This Source Code Form is subject to the terms of the Mozilla Public License (MPL), v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Filename:  scheduled_test_remove.php
 * 
 * Purpose:   Removes an OpenVigilance Task test in the database.
 *            The page is meant to be used as a simple web service, such as with Ajax or the like.
 * 
 * Output:    JSON array as defined in ``output_json_response.php''.
 * -------------------------------------------------------------------------------------------------------------- */

// Maintains the authenticated user session across different admin pages.
// Ensures only valid administrators can do anything.
session_start();

// Function: output_json_response($successful, $error_message)
require_once "output_json_response.php";

if(!isset($_SESSION['ov_admin_user'])) {
    output_json_response(false, "Not logged on as an administrator.");
    return;
}

// -------------------------------------------------------------------------------
// POST input variables sent by the web browser.
// Note: The login_code cannot be set by an OV admin. The code is auto-generated.
// -------------------------------------------------------------------------------
$sk = intval($_POST['remove_test_sk']);  // Surrogate Key identifier of the scheduled test in the database.
$subject_id = intval($_POST['remove_test_subject_id_hidden']);  // The ID of the person meant to take the test.
$test_condition = intval($_POST['remove_test_condition_hidden']);  // The kind of test being taken.
$login_code = strval($_POST['remove_login_code_hidden']);  // The 4-char login for the subject (user).

// Validate the data.
if($subject_id <= 0 || $test_condition <= 0) {
    output_json_response(false, "The subject_id nor the test_condition cannot be less than or equal to zero.");
    return;
}

else if($test_condition > 4) {
    output_json_response(false, "The test_condition cannot be greater than 4.");
    return;
}

else if(strlen($login_code) !== 4) {
    output_json_response(false, "The login_code must have four characters.");
    return;
}

// Logs into the OpenVigilance Task tests database to control and alter user tests.
// `$pdo' is defined as the database connection.
require_once "../settings.php";
require_once "../" . PROTECTED_SITE_CONFIGS_DIR . "openvigilance_db_connection_admin.php";

// Test Control: Remove the OV test in the database.
$sql = "DELETE FROM test_control WHERE sk=:sk AND subject_id=:subject_id AND test_condition=:test_condition AND login_code=:login_code";
$statement = $pdo->prepare($sql);
$statement->bindValue(":sk", $sk, PDO::PARAM_INT);
$statement->bindValue(":subject_id", $subject_id, PDO::PARAM_INT);
$statement->bindValue(":test_condition", $test_condition, PDO::PARAM_INT);
$statement->bindValue(":login_code", $login_code, PDO::PARAM_STR);
$successful_deletion = $statement->execute();  // true or false

if($successful_deletion) {
    // Inform the web browser that all went well and informs the Ajax client what the login_code is for the subject.
    output_json_response(true, ""); 
    return;
}

else {
    // Extra Data
    //$extra_data1 = 'sk = ' . strval($sk) .', subject_id = ' . strval($subject_id) .', test_condition = ' . strval($test_condition) . ', login_code = ' . strval($login_code);
    
    // The database was not properly updated for some reason.
    output_json_response(false, "The database could not be updated at this time for some reason.");
    return;
}
