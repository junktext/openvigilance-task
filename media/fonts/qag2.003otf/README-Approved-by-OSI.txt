The GUST Font Source License mentions that the font may be distributed under the
LaTeX Project Public License, which this license has been approved by the Open
Source Initiative (OSI) as a valid open source software license.

https://opensource.org/licenses/alphabetical
https://opensource.org/licenses/LPPL-1.3c
