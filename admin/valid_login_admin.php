<?php
/* --------------------------------------------------------------------------------------------------------------
 * Copyright (C) 2018 by William Paul Liggett (junktext@junktext.com)
 * This Source Code Form is subject to the terms of the Mozilla Public License (MPL), v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Filename:  validate_login_admin.php
 * 
 * Purpose:   Ensure that only administrators can see or change any OpenVigilance Task settings.
 * -------------------------------------------------------------------------------------------------------------- */

$valid_login_admin = false;

// Sets the `$ov_admin_username' and the `$ov_admin_password_hash' variables.
require_once "../settings.php";
require_once "../" . PROTECTED_SITE_CONFIGS_DIR . "openvigilance_admin_account.php";

// The `$submitted_username' and `$submitted_password' variables are from the calling PHP script.
// Also, the `$error_msg' is a standardized variable in all calling scripts to output the login error found.

// Determines if the login was valid or not.
if($submitted_username !== $ov_admin_username) {
    $error_msg = "Error: The username is not valid on this system.";
    return;
}

else if(!password_verify($submitted_password, $ov_admin_password_hash)) {
    $error_msg = "Error: The password is incorrect.";
    return;
}

// Valid login!
else {
    $valid_login_admin = true;
}
