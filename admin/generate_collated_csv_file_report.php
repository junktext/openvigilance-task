<?php
/* --------------------------------------------------------------------------------------------------------------
 * Copyright (C) 2018 by William Paul Liggett (junktext@junktext.com)
 * This Source Code Form is subject to the terms of the Mozilla Public License (MPL), v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Filename:  generate_collated_csv_file_report.php
 * 
 * Purpose:   Takes the raw CSV files that captured the random letter times and the user's activity to create
 *            a single, collated CSV file to help determine things such as the user's response time (RT) of how
 *            fast they reacted to a random letter that was displayed to them and whether they responded to the
 *            correct "O" letter.
 * 
 * Input:     Two raw CSV filenames as strings. The files must be named in a certain format, otherwise nothing 
 *            will happen.
 * 
 * Output:    A new collated CSV file titled in the manner of: 
 *            ov_subject_14_condition_3_on_20180330_1340_EDT_collated_file_report.csv
 * -------------------------------------------------------------------------------------------------------------- */

function generate_collated_csv_file_report($filename_raw_1, $filename_raw_2) {
    // Sanitizes the filename strings provided as input.
    $raw1_csv_filename_random_letter_times = htmlspecialchars($filename_raw_1, ENT_QUOTES, "UTF-8");
    $raw2_csv_filename_user_activity = htmlspecialchars($filename_raw_2, ENT_QUOTES, "UTF-8");
    
    // Sanitizes the filenames even more by ensuring they MUST match the format in a manner similar to:
    // 1st CSV = "ov_subject_14_condition_3_on_20180330_1340_EDT_raw_1_random_letter_times.csv"
    // 2nd CSV = "ov_subject_14_condition_3_on_20180330_1340_EDT_raw_2_user_activity.csv"
    //
    // This avoids have to blacklist every possible bad attack vector, like: ..  ;  /  \  (and so on).
    //
    // So, this determines the length of the filenames provided, as the only characteristic that should change with 
    // regards to the width of the string is the Subject ID. For example, an subject's ID might be 2, 14, 103, or the like.
    $string_width = strlen($raw1_csv_filename_random_letter_times);
    
    // Depending on the size of the filename, the various portion_#_start and portion_#_length will also change.
    //
    // These values do not deviate no matter what:
    $portion_2_start     = 11;
    $portion_3_length    = 11;
    $portion_4_length    = 1;
    $portion_5_length    = 4;
    $portion_6_length    = 8;
    $portion_7_length    = 1;
    $portion_8_length    = 4;
    $portion_9_length    = 1;
    $portion_10_length   = 3;
    $portion_11a_length  = 30;
    $portion_11b_length  = 24;
    
    // But, these values may change depending on how big the Subject ID number is:
    // Default size = 1 (e.g., The Subject ID is `1'.)
    $portion_2_length = 1;
    $portion_3_start  = 12;
    $portion_4_start  = 23;
    $portion_5_start  = 24;
    $portion_6_start  = 28;
    $portion_7_start  = 36;
    $portion_8_start  = 37;
    $portion_9_start  = 41;
    $portion_10_start = 42;
    $portion_11_start = 45;
    
    switch($string_width) {
        case 75:
            // e.g., Subject ID = 1
            // The values were already pre-set above, so we don't need to do anything further.
            break;
        
        case 76:
            // e.g., Subject ID = 12
            $portion_2_length = ++$portion_2_length;
            $portion_3_start  = ++$portion_3_start;
            $portion_4_start  = ++$portion_4_start;
            $portion_5_start  = ++$portion_5_start;
            $portion_6_start  = ++$portion_6_start;
            $portion_7_start  = ++$portion_7_start;
            $portion_8_start  = ++$portion_8_start;
            $portion_9_start  = ++$portion_9_start;
            $portion_10_start = ++$portion_10_start;
            $portion_11_start = ++$portion_11_start;
            break;
        
        case 77:
            // e.g., Subject ID = 123
            $portion_2_length = $portion_2_length + 2;
            $portion_3_start  = $portion_3_start + 2;
            $portion_4_start  = $portion_4_start + 2;
            $portion_5_start  = $portion_5_start + 2;
            $portion_6_start  = $portion_6_start + 2;
            $portion_7_start  = $portion_7_start + 2;
            $portion_8_start  = $portion_8_start + 2;
            $portion_9_start  = $portion_9_start + 2;
            $portion_10_start = $portion_10_start + 2;
            $portion_11_start = $portion_11_start + 2;
            break;
        
        case 78:
            // e.g., Subject ID = 1234
            $portion_2_length = $portion_2_length + 3;
            $portion_3_start  = $portion_3_start + 3;
            $portion_4_start  = $portion_4_start + 3;
            $portion_5_start  = $portion_5_start + 3;
            $portion_6_start  = $portion_6_start + 3;
            $portion_7_start  = $portion_7_start + 3;
            $portion_8_start  = $portion_8_start + 3;
            $portion_9_start  = $portion_9_start + 3;
            $portion_10_start = $portion_10_start + 3;
            $portion_11_start = $portion_11_start + 3;
            break;
        
        case 79:
            // e.g., Subject ID = 12345
            $portion_2_length = $portion_2_length + 4;
            $portion_3_start  = $portion_3_start + 4;
            $portion_4_start  = $portion_4_start + 4;
            $portion_5_start  = $portion_5_start + 4;
            $portion_6_start  = $portion_6_start + 4;
            $portion_7_start  = $portion_7_start + 4;
            $portion_8_start  = $portion_8_start + 4;
            $portion_9_start  = $portion_9_start + 4;
            $portion_10_start = $portion_10_start + 4;
            $portion_11_start = $portion_11_start + 4;
            break;
        
        default:
            // We'll assume there won't need to be more than 99,999 subject IDs.
            return "Error: The `Subject ID' is larger than 99,999 or the filename is invalid.";       
    }
    
    // Actually stores snippets of each filename as individual portions to analyze later.
    $portion_1a = substr($raw1_csv_filename_random_letter_times, 0, 11);                                    // "ov_subject_" (1st CSV)
    $portion_1b = substr($raw2_csv_filename_user_activity, 0, 11);                                          // "ov_subject_" (2nd CSV)
    $portion_2a = substr($raw1_csv_filename_random_letter_times, $portion_2_start, $portion_2_length);      // Subject ID (1st CSV)
    $portion_2b = substr($raw2_csv_filename_user_activity, $portion_2_start, $portion_2_length);            // Subject ID (2nd CSV)
    $portion_3a = substr($raw1_csv_filename_random_letter_times, $portion_3_start, $portion_3_length);      // "_condition_" (1st CSV)
    $portion_3b = substr($raw2_csv_filename_user_activity, $portion_3_start, $portion_3_length);            // "_condition_" (2nd CSV)
    $portion_4a = substr($raw1_csv_filename_random_letter_times, $portion_4_start, $portion_4_length);      // Test Condition # (1st CSV)
    $portion_4b = substr($raw2_csv_filename_user_activity, $portion_4_start, $portion_4_length);            // Test Condition # (2nd CSV)
    $portion_5a = substr($raw1_csv_filename_random_letter_times, $portion_5_start, $portion_5_length);      // "_on_" (1st CSV)
    $portion_5b = substr($raw2_csv_filename_user_activity, $portion_5_start, $portion_5_length);            // "_on_" (2nd CSV)
    $portion_6a = substr($raw1_csv_filename_random_letter_times, $portion_6_start, $portion_6_length);      // YYYYMMDD (1st CSV)
    $portion_6b = substr($raw2_csv_filename_user_activity, $portion_6_start, $portion_6_length);            // YYYYMMDD (2nd CSV)
    $portion_7a = substr($raw1_csv_filename_random_letter_times, $portion_7_start, $portion_7_length);      // "_" (1st CSV)
    $portion_7b = substr($raw2_csv_filename_user_activity, $portion_7_start, $portion_7_length);            // "_" (2nd CSV)
    $portion_8a = substr($raw1_csv_filename_random_letter_times, $portion_8_start, $portion_8_length);      // HHMM (1st CSV)
    $portion_8b = substr($raw2_csv_filename_user_activity, $portion_8_start, $portion_8_length);            // HHMM (2nd CSV)
    $portion_9a = substr($raw1_csv_filename_random_letter_times, $portion_9_start, $portion_9_length);      // "_" (1st CSV)
    $portion_9b = substr($raw2_csv_filename_user_activity, $portion_9_start, $portion_9_length);            // "_" (2nd CSV)
    $portion_10a = substr($raw1_csv_filename_random_letter_times, $portion_10_start, $portion_10_length);   // Timezone (1st CSV)
    $portion_10b = substr($raw2_csv_filename_user_activity, $portion_10_start, $portion_10_length);         // Timezone (2nd CSV)
    $portion_11a = substr($raw1_csv_filename_random_letter_times, $portion_11_start, $portion_11a_length); // "_raw_1_random_letter_times.csv" (1st CSV)
    $portion_11b = substr($raw2_csv_filename_user_activity, $portion_11_start, $portion_11b_length);       // "_raw_2_user_activity.csv" (2nd CSV)
    
    // Ultimately, this needs to be `true', which we'll set to `false' if any sanitation filter fails.
    $filenames_passes_sanitation_test = true;
    
    // Portion 10: Timezone (Doing this check first as it's not part of the if-else logic below.
    try {
        // These will cause an Exception if the timezone abbreviation is invalid.
        $timezone_a = new DateTimeZone($portion_10a);
        $timezone_b = new DateTimeZone($portion_10b);
    } 
    
    catch(\Exception $e) {
        $filenames_passes_sanitation_test = false;
    }
    
    // Portion 1: "ov_subject_"
    if(($portion_1a !== "ov_subject_") && ($portion_1b !== "ov_subject_")) {
        $filenames_passes_sanitation_test = false;
    }
    
    // Portion 2: Subject ID
    else if(!is_numeric($portion_2a) && !is_numeric($portion_2b)) {
        $filenames_passes_sanitation_test = false;
    }
    
    // Portion 3: "_condition_"
    else if(($portion_3a !== "_condition_") && ($portion_3b !== "_condition_")) {
        $filenames_passes_sanitation_test = false;
    }
    
    // Portion 4: Test Condition #
    else if(!is_numeric($portion_4a) && !is_numeric($portion_4b)) {
        $filenames_passes_sanitation_test = false;
    }
    
    // Portion 5: "_on_"
    else if(($portion_5a !== "_on_") && ($portion_5b !== "_on_")) {
        $filenames_passes_sanitation_test = false;
    }
    
    // Portion 6: YYYYMMDD
    else if(!is_numeric($portion_6a) && !is_numeric($portion_6b)) {
        $filenames_passes_sanitation_test = false;
    }
    
    // Portion 7: "_"
    else if(($portion_7a !== "_") && ($portion_7b !== "_")) {
        $filenames_passes_sanitation_test = false;
    }
    
    // Portion 8: HHMM
    else if(!is_numeric($portion_8a) && !is_numeric($portion_8b)) {
        $filenames_passes_sanitation_test = false;
    }
    
    // Portion 9: "_"
    else if(($portion_9a !== "_") && ($portion_9b !== "_")) {
        $filenames_passes_sanitation_test = false;
    }
    
    // Portion 10: Timezone (Already tested for this before the if-else logic.)
    
    // Portion 11a: "_raw_1_random_letter_times.csv"
    else if($portion_11a !== "_raw_1_random_letter_times.csv") {
        $filenames_passes_sanitation_test = false;
    }
    
    // Portion 11b: "_raw_2_user_activity.csv"
    else if($portion_11b !== "_raw_2_user_activity.csv") {
        $filenames_passes_sanitation_test = false;
    }
    
    // Bad filenames! So, we won't do anything with them.
    if(!$filenames_passes_sanitation_test) {
        return "Error: The filename for either the 1st or 2nd CSV file did not pass the validation rules.";
    }
    
    // Good filenames! Let's move on.
    else {
        try {
            $filepath = "../user-data-files/raw-input/";
            
            // Tries to open the raw CSV files in read-only mode.
            $csv_file_1 = @fopen($filepath . $raw1_csv_filename_random_letter_times, "r");
            $csv_file_2 = @fopen($filepath . $raw2_csv_filename_user_activity, "r");
            
            if(!$csv_file_1) {
                throw new Exception("Cannot open: " . $raw1_csv_filename_random_letter_times);
            } 
            
            else if(!$csv_file_2) {
                throw new Exception("Cannot open: " . $raw2_csv_filename_user_activity);
            }
            
            // Empty PHP arrays that we'll build-up.
            $php_array_raw1 = array();
            $php_array_raw2 = array();
            
            // Reads the 1st CSV file and builds-up a native PHP array.
            // We'll just copy and paste everything as each letter is incremented by 1 for every GlobalTrialCounter.
            while(($row = fgetcsv($csv_file_1)) !== FALSE) {
                array_push($php_array_raw1, $row);
            }
            
            // Reads the 2nd CSV file and builds-up a native PHP array.
            // This file is different, as it only records data whenever the user responded. So, the GlobalTrialCounter
            // reference in this file will mean what random letter was shown when the user responded.
            while(($row = fgetcsv($csv_file_2)) !== FALSE) {
                if($row[0] === "GlobalTrialCounter") {
                    // Title heading of: GlobalTrialCounter and ov_participant_response_time
                    // Mainly only useful for debugging purposes.
                    array_push($php_array_raw2, $row);
                }
                
                // Since PHP is awesome, we can create the array to index it as it shows up in the GlobalTrialCounter
                // data for the 2nd CSV. This way, $php_array_raw1[42] and $php_array_raw2[42] will refer to the same letter! :-)
                // Specifically, we'll have this raw2 array stored in the format of:
                // 
                //   GlobalTrialCounter  = ov_participant_response_time
                //   ------------------    -----------------------------
                //   $php_array_raw2[42] = 1522270448181
                $php_array_raw2[$row[0]] = $row[1];
            }
            
            // Closes the raw CSV files.
            fclose($csv_file_1);
            fclose($csv_file_2);
            
            // ------------------------------------------------------------------------------------------------------------
            // Builds-up the collated array to what we want the single, collated CSV file to report.
            $php_array_collated = array();
            
            // It'll write the collated CSV file report using the raw1 (random letter times) for each row, and then
            // determine if the user responded to a particular letter and how fast they did so using the raw2 file.
            // The collated CSV will have the following column title headings:
            // 
            //   * SID:                 Subject ID
            //   * TimeStamp:           The time a random letter was shown.
            //   * Type:                Test type ("P" for Practice, or "E" for Experiment).
            //   * Block:               Each test is divided into equal blocks for analysis. Default: Block # increases every 2-minutes.
            //   * GlobalTrialCounter:  The unique instance of every random letter. Also, increments during the practice test.
            //   * CurrentTrial:        Same idea as the GlobalTrialCounter, except that the number does not include the practice test.
            //   * Target:              The target is the letter "O". So, if the value is 1 then it was the letter "O", but 0 if not.
            //   * Distractor:          The opposite of the target. So, a distractor is either the letter "D" or backwards "D".
            //   * FalseAlarm:          If the participant responds to the distractor then this value will be 1.
            //   * RT:                  Response Time (ms) of the participant. This value is > 0 if a user responded. For both the Target and Distractor.
            //   * Correct:             If the participant responds to the letter "O" in less than one second, this value is 1. It's 0 if they guess wrong or don't respond in time.
            $title_headings = [
                "SubjectID",
                "TestCondition",
                "TimeStamp", 
                "Type", 
                "Block", 
                "GlobalTrialCounter", 
                "CurrentTrial", 
                "Target", 
                "Distractor", 
                "FalseAlarm",
                "RT",
                "Correct"
            ];
            
            array_push($php_array_collated, $title_headings);
            
            // Subject ID from the validated filename information.
            $subject_id = $portion_2a;
            
            // Same thing for Test Condition #
            $test_condition = $portion_4a;
            
            for($i = 1; $i < count($php_array_raw1); $i++) {
                // GlobalTrialCounter
                $global_trial_counter = $php_array_raw1[$i][0];
                
                // TimeStamp in milliseconds.
                $time_stamp = $php_array_raw1[$i][2];
                
                // ov_random_letter_is_o
                $ov_random_letter_is_o = $php_array_raw1[$i][5];
                
                // Target and Distractor
                if($ov_random_letter_is_o === "1") {
                    $target = 1;
                    $distractor = 0;
                }
                
                else {
                    $target = 0;
                    $distractor = 1;
                }
                
                // User's Response Time (RT) in milliseconds.
                // ov_participant_response_time - TimeStamp
                if(array_key_exists($i, $php_array_raw2)) {
                    $rt = $php_array_raw2[$i] - $time_stamp;
                }
                
                else {
                    // The user did not respond to this random letter.
                    $rt = 0;
                }
                
                // FalseAlarm
                if(($rt > 0) && ($distractor === 1)) {
                    $false_alarm = 1;
                }
                
                else {
                    $false_alarm = 0;
                }
                
                // Correct (Did the user respond or not respond appropriately to the random letter shown?)
                if($false_alarm) {
                    $correct = 0;
                }
                
                else if(($rt === 0) && ($target === 1)) {
                    $correct = 0;
                }
                
                else {
                    $correct = 1;
                }
                
                // We'll now change what is shown for the TimeStamp to get rid of the Unix time in milliseconds to
                // convert something like "1522270446740" to "2018-03-28 20:54:06.740"
                // PHP doesn't handle Unix time in milliseconds natively, as it uses either seconds or microseconds.
                // So, we'll force it to use the millisecond format like JavaScript reported it.
                $time_stamp_output = date("Y-m-d H:i:s", substr($time_stamp, 0, 10)) . "." . substr($time_stamp, 10, 3);
                
                $collated_row = [
                    $subject_id,             // SID
                    $test_condition,         // Test Condition #
                    $time_stamp_output,      // TimeStamp
                    $php_array_raw1[$i][3],  // Type
                    $php_array_raw1[$i][4],  // Block
                    $global_trial_counter,   // GlobalTrialCounter
                    $php_array_raw1[$i][1],  // CurrentTrial
                    $target,                 // Target
                    $distractor,             // Distractor
                    $false_alarm,            // FalseAlarm
                    $rt,                     // Response Time (RT)
                    $correct                 // Correct
                ];
                
                array_push($php_array_collated, $collated_row);
            }
            
            // ------------------------------------------------------------------------------------------------------------
            // Creates the single, collated CSV file report and stores it on the web server.
            $filepath = "../user-data-files/collated/";

            // Filename Prefix: E.g., "ov_subject_14_condition_3_on_20180330_1340_EDT"
            $filename_prefix = substr($raw1_csv_filename_random_letter_times, 0, $portion_11_start);
            $filename_suffix = "_collated_file_report.csv";
            $filename = $filename_prefix . $filename_suffix;
            
            // Check if a previous collated CSV report needs to be generated again.
            if(file_exists($filepath . $filename)) {
                // Sets the file to be writable again, since we want to over-write it.
                chmod($filepath . $filename, 0644);
            }
            
            $new_collated_csv_file = fopen($filepath . $filename, "w");

            // Checks whether the file pointer is valid.
            if(!$new_collated_csv_file) {
                throw new Exception("Cannot write new collated CSV file: " . $filename);
            }

            foreach($php_array_collated as $csv_data) {
                fputcsv($new_collated_csv_file, $csv_data);
            }

            fclose($new_collated_csv_file);

            // Sets the file to be read-only to indicate that no data should ever be modified directly later on!
            chmod($filepath . $filename, 0444);
        
            // Success!
            return true;
        } 
        
        catch(\Exception $e) {
            return $e->getMessage();
        } 
    }
}