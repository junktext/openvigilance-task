// @license magnet:?xt=urn:btih:3877d6d54b3accd4bc32f8a48bf32ebc0901502a&dn=mpl-2.0.txt MPL-2.0

/* ------------------------------------------------------------------------------------------------------------------ 
    Copyright (C) 2018 by William Paul Liggett (junktext@junktext.com)
    This Source Code Form is subject to the terms of the Mozilla Public License (MPL), v. 2.0.
    If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.    
------------------------------------------------------------------------------------------------------------------ */

// ------------------
//  GLOBAL VARIABLES
// ------------------
// Determines when to record the user's activity, such as when they press the space bar during a test.
var ov_record_user_activity = false; 

// Random Letter Log: Documents various facts about each random letter shown. This is half of the data we need collected.
// The other crucial aspect is the User Activity Log, but we'll capture this aspect in its own array.
// Both sets of data will eventually be submitted to the server, after which the server will pair up the data values correctly. 
// Note: We don't need the following cells as we can create the information on the server: "SID", "Target", "Distractor", "FalseAlarm", and "Correct"
// The heading of each cell is listed below, but the heading will not be repeated for new rows.
// Furthermore, the "GlobalTrialCounter" will effectively serve as the row number!
var ov_random_letter_log = [["GlobalTrialCounter", "CurrentTrial", "TimeStamp", "Type", "Block", "ov_random_letter_is_o"]];

// User Activity Log: Documents the datetimes of every occurrence of when the user pressed the space bar. This is the other half
// of the data we need collected. The other crucial aspect is the Random Letter Log.
// FYI when "GlobalTrialCounter = 0": A user can generate activity entries after the test starts but before a random letter is 
// shown. This is because there is a small delay between when the letter is visually presented inititially.
// Moreover, a user can generate activity entries multiple times on the same "GlobalTrialCounter" number. This is because the
// random letter changes at the pace of EVENTS_PER_MINUTE (default: 57.5 events = 958.33 ms) and the user is allowed to press
// the space bar as many times as they want. Lastly, if the user holds down the space bar, an activity entry is generated at
// however fast the operating system (OS) sends repeating keystrokes when holding a key down. Note: A user can easily modify 
// this rate within their profile on their OS. It's just a setting in their control panel.
var ov_user_activity_log = [["GlobalTrialCounter", "ov_participant_response_time"]];

// Tracks whether it is a practice test ("P") or an actual experiment ("E").
var ov_test_type = "";  

// A test block are 2-minute periods of time during the actual test.
// Note: The practice test should always be less than 2-mins or further calculations will be messed up.
// Block 0 = Practice. Block 1 = 1st 2-min period. Block 2 = 2nd 2-min period. Block 3 = 3rd 2-min period. And so on. 
var ov_test_block = -1;

// A test block is a subset of a 12-min test. For now, each block is divided into 2-min periods.
const OV_TEST_BLOCK_EACH_DURATION = parseFloat(120000); // 2 minutes = 120,000 ms

// Used to increment the `ov_test_block' var every 2-minutes when needed by identifying the setInterval() ID.
var ov_test_block_timer_id = -1;

// Primarily used to outputting the time of the current random letter to the CSV.
//var ov_current_random_letter_time_as_string = "";

// This is used to measure the user's response time (RT). Eventually uses Unix epoch time format (ms).
var ov_current_random_letter_datetime = -1;

// Quick way to determine how to log certain fields in the CSV, such as: Target, Distractor, False Alarm, and Correct.
// Either: 0 (false) or 1 (true)
var ov_random_letter_is_o = 0;

// Global Trial Counter: [Practice Test] + [Actual Test]
// Increments by one each time a new random letter is shown to the user. Eventually output in the CSV.
var ov_global_trial_counter = 0;

// Current Trial Counter: ONLY the Actual Test, as the Practice Test will just log zeroes.
// Increments by one each time a new random letter is shown to the user. Eventually output in the CSV.
var ov_current_trial_counter = 0;

// Response Time (RT): Measure the time in ms of how long it took them to press the space bar after a random letter was displayed.
// Note: It doesn't matter if the random letter was the correct letter 'O' as the RT is logged whenever the user presses the 
// space bar or is set to -1 to indicate that the user did not respond to a random letter.
var ov_participant_response_time = -1;

;(function(window, $) {
    // 50 ms display duration for the letter to appear and then disappear on the screen.
    const DISPLAY_LETTER_DURATION = 50;
    
    // 57.5 events per minute. An event is a random letter change.
    // (57.5/60) * 1000 = 958.333333333 ms  [Note: 1000 ms = 1 second]
    const EVENTS_PER_MINUTE = (57.5/60) * 1000;
    
    // Countdown Timer: Used during the breaks between vigilance tasks.
    function countdownTimer(seconds_remaining = 30) {        
        // Efficient jQuery selectors.
        var ov_timer_minutes = $("#ov_timer_minutes");
        var ov_timer_minutes_label = $("#ov_timer_minutes_label");
        var ov_timer_seconds = $("#ov_timer_seconds");
                                             
        // Gets the current time (ms since Unix epoch) and adds the seconds remaining (in ms).
        var countdown_to_time = Date.now() + (seconds_remaining * 1000);
        
        // Performs the countdown, updates the display, and does this every 1 second.
        var countdown_interval = setInterval(function() {  
            // Unix epoch format (in ms).
            var remaining_time_in_ms = countdown_to_time - Date.now();
            
            // Converts the remaining time to a Date() object and tracks the minutes and seconds left.
            var remaining_time_object = new Date(remaining_time_in_ms);
            var minutes_left = remaining_time_object.getMinutes();
            var seconds_left = remaining_time_object.getSeconds();
            
            // If the remaining time is within the 1 minute range, then it'll say "minute". Otherwise it'll say "minutes".
            // This is because it sounds better to say "1 minute", "2 minutes", and so on.
            if(remaining_time_in_ms <= 120000 && remaining_time_in_ms >= 60000) {
                ov_timer_minutes_label.html(" minute and ");
            } 
            
            // If the time is less than 1 minute, then we'll remove the "X minutes" display to only show the seconds remaining.
            else if(remaining_time_in_ms <= 60000) {
                ov_timer_minutes.html("");
                ov_timer_minutes_label.html("");
                minutes_left = "";
            }
            
            // For any time at or above 2 minutes, it'll show: X minutes and Y seconds
            else {
                ov_timer_minutes_label.html(" minutes and ");
            }
            
            // Stops the countdown timer at 0 minutes and 0 seconds.
            if(remaining_time_in_ms <= 1000) {
                clearInterval(countdown_interval);
            }
            
            // Outputs the time left to the user.
            ov_timer_minutes.html(minutes_left);
            ov_timer_seconds.html(seconds_left);
        }, 1000);  // The timer updates every 1000 ms (1 sec).
    }
    
    // Function by Mozilla Contributors, Feb 2018
    // Source:  https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    // License: CC By-SA 2.5 [http://creativecommons.org/licenses/by-sa/2.5/]
    //
    // Usage: getRandomInt(3));  // Output: 0, 1 or 2
    function getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }

    // changeRandomLetter(): Displays a random letter and then logs the timing data of when it was displayed to an array.
    // The chosen random letter is first determined and then, eventually, the letter will be displayed to the user in a 
    // later code block. Note: The `chosen_letter' var will match the CSS class ID in the HTML, as we'll use this later 
    // for the duration the letter is displayed.
    function changeRandomLetter() {
        var random_int = getRandomInt(3);
        var chosen_letter = ""; 
        
        // Global Trial Counter: [Practice Test] + [Actual Test]
        // Increments by one each time a new random letter is shown to the user. Eventually output in the CSV.
        if(ov_record_user_activity === true) {
            ++ov_global_trial_counter;
        }
        
        // Current Trial Counter: ONLY the Actual Test (experiment), as the Practice Test will just log zeroes.
        if((ov_record_user_activity === true) && (ov_test_type === "E")) {
            ++ov_current_trial_counter;
        }
        
        // Letter: O
        if(random_int === 0) {
            chosen_letter = "letter_o";
            ov_random_letter_is_o = 1;  // Reports to the global var which is used in the user's activity log.

            // Hides the letter D.
            var letter_d_hidden = $("span.letter_d").hasClass("hidden");
            if(letter_d_hidden === false) {
                $("span.letter_d").addClass("hidden");
            } 

            // Hides the letter backwards D.
            var letter_backwards_d_hidden = $("span.letter_backwards_d").hasClass("hidden");
            if(letter_backwards_d_hidden === false) {
                $("span.letter_backwards_d").addClass("hidden");
            }
       }

        // Letter: D
        else if(random_int === 1) {
            chosen_letter = "letter_d";
            ov_random_letter_is_o = 0;  // Reports to the global var which is used in the user's activity log.

            // Hides the letter O.
            var letter_o_hidden = $("span.letter_o").hasClass("hidden");
            if(letter_o_hidden === false) {
                $("span.letter_o").addClass("hidden");
            } 

            // Hides the letter backwards D.
            var letter_backwards_d_hidden = $("span.letter_backwards_d").hasClass("hidden");
            if(letter_backwards_d_hidden === false) {
                $("span.letter_backwards_d").addClass("hidden");
            }
       }

        // Letter: Backwards D
        else {
            chosen_letter = "letter_backwards_d";
            ov_random_letter_is_o = 0;  // Reports to the global var which is used in the user's activity log.

            // Hides the letter D.
            var letter_d_hidden = $("span.letter_d").hasClass("hidden");
            if(letter_d_hidden === false) {
                $("span.letter_d").addClass("hidden");
            } 

            // Hides the letter O.
            var letter_o_hidden = $("span.letter_o").hasClass("hidden");
            if(letter_o_hidden === false) {
                $("span.letter_o").addClass("hidden");
            }
        }
        
        // Reports the time in ms since the Unix epoch, such as: 1520215907134.
        ov_current_random_letter_datetime = Date.now();
        
        // Displays the random letter for only a short duration (e.g., 50 ms).
        // Shows the letter briefly.
        $("span." + chosen_letter).removeClass("hidden");
        
        setTimeout(function() {
            // Hides the letter once the duration length is reached.
            $("span." + chosen_letter).addClass("hidden");
        }, DISPLAY_LETTER_DURATION);
        
        // Logs the activity to the JS array as a new row for each letter shown to the user.
        if(ov_record_user_activity === true) {
            // Note: The `ov_subject_id' var was defined already by the PHP script.
            // Also, the "GlobalTrialCounter" will effectively serve as the row number!
            // The title headings are:   "GlobalTrialCounter",        "CurrentTrial",               "TimeStamp",                 "Type",       "Block",    "ov_random_letter_is_o"
            ov_random_letter_log.push([ov_global_trial_counter, ov_current_trial_counter, ov_current_random_letter_datetime, ov_test_type, ov_test_block, ov_random_letter_is_o]);
        }
    }
    
    // Sets a continual timer to change the random letter displayed.
    // An event is a random letter change set to the rate of EVENTS_PER_MINUTE.
    setInterval(changeRandomLetter, EVENTS_PER_MINUTE);
    
    // Shows the vigilance test (small background circles and flashing random letters) when needed.
    function showVigilanceDisplay(banner_text = "", hide_nature_video = false) {
        // Let's the primary keydown() script know that any space bar presses should be counted in the user activity array.
        // The only reason this should be set to `false' is for Test Condition #4 as the break still shows random letter,
        // but we're not supposed to record the user's activity in this instance.
        ov_record_user_activity = true;
        
        // Outputs the "Practice Test" banner.
        if(banner_text === "Practice Test") {
            $("#vigilance_display_banner_text").html(banner_text);  // This is a Practice Test.
            $("#vigilance_display_extra_text").html("&nbsp;");
            ov_test_type = "P";  // Practice Test. Included as part of the user's activity array.
            ov_test_block = 0;  // The practice test block is always 0.
        }
        
        // Test Condition #4: We want the random letters to display, but we don't want to record the user's activity!
        // The `banner_text' will say... Break: X minutes and Y seconds remaining
        else if(banner_text !== "") {
            ov_record_user_activity = false;
            
            // Stops incrementing the test block number every 2-min until a showVigilanceDisplay() without a break is called again.
            clearInterval(ov_test_block_timer_id);

            $("#vigilance_display_banner_text").html(banner_text);
            $("#vigilance_display_extra_text").html("The test will automatically begin after the break. Please be ready.<br />" +
                                                    "Again, press the <strong>space bar</strong> only when you see the letter " +
                                                    "<strong>O</strong>, not for any other letter.");
        }
        
        // This is a real test/experiment.
        else {
            $("#vigilance_display_banner_text").html("&nbsp;");
            $("#vigilance_display_extra_text").html("&nbsp;");
            ov_test_type = "E";  // Actual Test (experiment). Included as part of the user's activity array.
            
            // Increments the test block number and then does this every 2-min until a hideVigilanceDisplay() is called later.
            ++ov_test_block;
            ov_test_block_timer_id = setInterval(function() { ++ov_test_block; }, OV_TEST_BLOCK_EACH_DURATION);
        }
        
        // For: Test Condition #3. Hides the nature video.
        if(hide_nature_video) {
            var nature_video = $("#nature_video").get(0);
            nature_video.pause();
            $("#nature_video_div").addClass("hidden").css({"display": "none"});
        }
        
        // Shows the flashing random letters and small background circles to the user.
        $("#VigilanceDisplay").removeClass("hidden");
    }
    
    // Hides the vigilance test (small background circles and flashing random letters) when needed.
    function hideVigilanceDisplay(banner_text = "", extra_text = "", play_nature_video = false) {
        // Let's the primary keydown() script know that any space bar presses should NOT be counted in the user activity array.
        ov_record_user_activity = false;
        
        // Stops incrementing the test block number every 2-min until a showVigilanceDisplay() is called again.
        clearInterval(ov_test_block_timer_id);
        
        // Outputs the "Break" banner when needed.
        if(banner_text !== "") {
            // Default message between a break. Typically used for the larger 4-min break in-between two 12-min tasks.
            // But, it was requested to provide a different message in-between the Practice Test and the Actual Test.
            if(extra_text === "") {
                extra_text = "The test will automatically begin after the break. Please be ready.<br />" +
                             "Again, press the <strong>space bar</strong> only when you see the letter " +
                             "<strong>O</strong>, not for any other letter.";
            }
            
            $("#vigilance_display_banner_text").html(banner_text);
            $("#vigilance_display_extra_text").html(extra_text);
        }
        
        else {
            $("#vigilance_display_banner_text").html("&nbsp;");
            $("#vigilance_display_extra_text").html("&nbsp;");
        }
        
        // Hides the flashing random letters and small background circles to the user.
        $("#VigilanceDisplay").addClass("hidden");
        
        // For: Test Condition #3. Plays the nature video.
        if(play_nature_video) {
            $("#nature_video_div").removeClass("hidden").css({"display": "block"});
            var nature_video = $("#nature_video").get(0);
            nature_video.volume = 1.0;
            nature_video.play();
        }
    }
    
    // Stops the test and submits the user's activity array (space bar responses) to the server.
    function concludeTest() {
        ov_record_user_activity = false;
        clearInterval(ov_test_block_timer_id);
        $("#VigilanceDisplay").remove();
        $("#vigilance_display_banner_text").html("Submitting the test data...");
        $("#vigilance_display_extra_text").html("Please wait to ensure your test data has been processed correctly.");
        
        // If any kind of Ajax error occurs at all, the script may get to a weird point where it won't properly inform the
        // participant that there was a problem as there are mulitple Ajax requests going on. Therefore, if this variable is
        // ever set to `true' then we'll make sure to activate the report_ajax_error() function to be on the safe-side.
        var ajax_any_error_whatsoever = false;
        
        // Used to monitor how many times the report_ajax_error() function was called from within an $.ajax() function.
        var report_ajax_error_func_used_count = 0;
        
        // If an Ajax error occurs, the report_ajax_error() function may not see the full breadcrumb trail of Ajax responses.
        // This is used to build up a string of each Ajax response, to include this in the report_ajax_error() details.
        var ajax_sequence_response_trail = "<br /><br />";
        
        // Ensures that if the user's PHP session timed out that we'll log them back in if they have a valid account still.
        // The JS vars were pre-set by PHP already as embedded source code for the user's visit.
        var ov_user_account = [ov_database_user_sk, ov_subject_id, ov_test_condition, ov_login_code];
        
        // The raw input data contains both the random letter times and when the user responded. Each data portion has been
        // converted from an array to a flattened string to avoid reaching PHP `max_input_vars' setting. In this case, the
        // single array of two strings will only take up 2 input vars!
        var ov_raw_input_data = [ov_random_letter_log.toString(), ov_user_activity_log.toString()];
        
        // Internal function only used if there was an Ajax failure for some reason.
        function report_ajax_error(detailed_error_message = "") {
            ajax_any_error_whatsoever = true;
            report_ajax_error_func_used_count++;
            
            // Rarely this might be blank, such that an Ajax error occured but then another Ajax request completed and all
            // looks well when this isn't really the case. So, we'll at least provide a few details to help with debugging.
            if(detailed_error_message === "") {
                detailed_error_message = "Number of Ajax errors: " + report_ajax_error_func_used_count + 
                                         ajax_sequence_response_trail;
            }
            
            // Converts the User Activity Log/Random Letter Log arrays into strings.
            // Explains to the user they should save the page as an ".html" file and to email it to William.
            $("#vigilance_display_banner_text").html("Network Failure");
            $("#vigilance_display_extra_text").html(
                "Your test data was NOT submitted to the server due to some network problem. " +
                "However, your information has been stored locally. Please save this page as an \".html\" file and email it to: " +
                "<a href=\"mailto:junktext@junktext.com\">junktext@junktext.com</a>. Your test data will then be manually processed. " +
                "We apologize for the inconvenience!</p>" + 
                "<p><strong>TEST DATA CAPTURED:</strong> <br /><br />" + 
                "    var string_ov_random_letter_log = \"" + ov_random_letter_log.toString() + "\";" +
                "    <br /><br />" +
                "    var string_ov_user_activity_log = \"" + ov_user_activity_log.toString() + "\";" +
                "    <br /><br />" +
                "    var ov_database_user_sk = " + ov_database_user_sk + ";" +
                "    <br /><br />" +
                "    var ov_subject_id = " + ov_subject_id + ";" +
                "    <br /><br />" +
                "    var ov_test_condition = " + ov_test_condition + ";" +
                "<br /><br /></p>" +
                "<p><strong>DETAILED ERROR MESSAGE:</strong><br /><br />" + detailed_error_message
            ).css(
                {
                    // Changes the CSS to make the longer messages to the user more readable.
                    "width": "100%",
                    "display": "block",
                    "margin": "auto",
                    "text-align": "left"
                }
            );
    
            // Halts the rest of the JavaScript program here if anything went awry.
            throw new Error("An Ajax error occurred at calling location:" + detailed_error_message);
        }
        
        // Confirms the user is logged in and then submits the raw input data that was collected.
        $.ajax({
           type:        "POST",
           url:         "admin/process_completed_test_user_data.php",
           data:        {ov_user_account: ov_user_account},
           success:     function(response) {
                            ajax_sequence_response_trail += "1st Ajax Response: " + response + "<br /><br />";
                            
                            // Submits the JS array `ov_user_activity_log' to the server.
                            $.ajax({
                                type:        "POST",
                                url:         "admin/process_completed_test_user_data.php",
                                data:        {ov_raw_input_data: ov_raw_input_data },
                                success:     function(response, textStatus, jqXHR) {
                                                ajax_sequence_response_trail += "2nd Ajax Response: " + response + "<br /><br />";
                                                
                                                var response_as_json = JSON.parse(response);
                                    
                                                if(response_as_json.successful !== true) {                                                   
                                                    report_ajax_error("OV Error Code: #AJ001<br />jqXHR: " + jqXHR.toString() + 
                                                                      "<br />textStatus: " + textStatus + "<br />JSON response: " + response +
                                                                      ajax_sequence_response_trail);
                                                }
                                             },
                                
                                error:      function(jqXHR, textStatus, errorThrown) {
                                                report_ajax_error("OV Error Code #AJ002<br />jqXHR: " + jqXHR.toString() + 
                                                                  "<br />textStatus: " + textStatus + "<br />errorThrown: " + errorThrown +
                                                                  ajax_sequence_response_trail);
                                            }
                             });
                        },
                        
            error:      function(jqXHR, textStatus, errorThrown) {
                            report_ajax_error("OV Error Code #AJ003<br />jqXHR: " + jqXHR.toString() + 
                                             "<br />textStatus: " + textStatus + "<br />errorThrown: " + errorThrown +
                                             ajax_sequence_response_trail);
                        }
        });
        
        // Rarely needed, but essential if something goes weird with submitting the data to the server via Ajax.
        // Ensures that if an Ajax error occurred, that we don't accidentally report that the CSV files were created correctly.
        if((ajax_any_error_whatsoever) && $("#vigilance_display_banner_text").html() !== "Network Failure") {
            report_ajax_error();
        }
        
        // Success! Let's end the participant's test.
        else {
            $("#vigilance_display_banner_text").html("Test Complete");
            $("#vigilance_display_extra_text").html("Thank you very much for participating!");
            
            // Shows the participant's "Sign Out" button to allow another participant to sign-in on the same computer.
            $("#user_sign_out").removeClass("hidden");
        }
    }
    
    $(document).ready(function() {
        // For: Test Condition #3. Ensures that the entire nature video is downloaded, since a user may get disconnected!
        if(ov_test_condition === 3) {
            // Script code modified from: http://dinbror.dk/blog/how-to-preload-entire-html5-video-before-play-solved/
            var nature_video_download_complete = false;
            $("#StartTestButton").val("Please wait. Downloading assets...");

            var req = new XMLHttpRequest();
            
            // The video clip is from: https://www.youtube.com/watch?v=MjCorj48dEw
            // License: Creative Commons Attribution
            // Clip title: National Geographic 2018 | The Most Peaceful Places on Earth | New Documentary 2018
            // YouTube Channel: Breakthrough GO (https://www.youtube.com/channel/UCYuVc2_bO0HAWpqVvF2Mr-A)
            // Published on YouTube: January 1st, 2018
            // Clip was shortended to a 4-minute segment by William Paul Liggett (2018).
            req.open('GET', 'media/OpenVigilance_Nature_Video.mp4', true);
            req.responseType = 'blob';

            req.onload = function() {
               // `onload' is triggered even on 404. So, we need to check the status code.
               if(this.status === 200) {
                  var videoBlob = this.response;
                  var vid = URL.createObjectURL(videoBlob); // IE10+
                  
                  // Video is now downloaded! We can set it as source on the video element.
                  var nature_video = $("#nature_video").get(0);
                  nature_video.src = vid;

                  nature_video_download_complete = true;
                  $("#StartTestButton").val("Start Test");
               }
            };

            req.onerror = function() {
               // Error
            };

            req.send();
        }
        
        // For: Test Condition #4. Ensures that the user has the `break_reminder_for_test_condition_4.png', 
        // since a user may get disconnected!
        if(ov_test_condition === 4) {
            // Script code modified from: http://dinbror.dk/blog/how-to-preload-entire-html5-video-before-play-solved/
            var break_reminder_download_complete = false;
            $("#StartTestButton").val("Please wait. Downloading assets...");

            var req = new XMLHttpRequest();
            
            // Break Reminder Image created by William Paul Liggett (2018).
            req.open('GET', 'media/break_reminder_for_test_condition_4.png', true);
            req.responseType = 'blob';

            req.onload = function() {
               // `onload' is triggered even on 404. So, we need to check the status code.
               if(this.status === 200) {
                  var imageBlob = this.response;
                  var image = URL.createObjectURL(imageBlob); // IE10+
                  
                  // Image is now downloaded! We'll now add it to the DOM.
                  $("#break_reminder_popup").append("<img id=\"break_reminder_popup_image\" src=\"media/break_reminder_for_test_condition_4.png\" alt=\"Break Reminder Pop-Up\" />");

                  break_reminder_download_complete = true;
                  $("#StartTestButton").val("Start Test");
               }
            };

            req.onerror = function() {
               // Error
            };

            req.send();
        }
        
       // Records the user's reaction time when they press the space bar if a vigilance test is running.
       $("body").keydown(function(event) {
           // The space bar character in ASCII is #32.
           if((event.which === 32) && (ov_record_user_activity === true)) {
               // The user's pressed the space bar at this time in ms (Unix epoch from 1970).
               ov_participant_response_time = Date.now();
               
               // User Activity Log: Tracks the time of when the user pressed the space bar.
               ov_user_activity_log.push([ov_global_trial_counter, ov_participant_response_time]);
           }
           
           // Test Condition #4: Reminds the participant that a break is occurring by showing a pop-up image.
           else if((event.which === 32) && (ov_test_condition === 4) && 
                   (ov_record_user_activity === false) && (ov_test_block > 0)) {
               
               // Displays the break reminder pop-up image.
               $("#break_reminder_popup").removeClass("hidden").css({"display": "block"});
               
               // Waits 3.5 seconds before fading out.
               setTimeout(function() { $("#break_reminder_popup").fadeOut(); }, 3500);
           }
       });
        
       // Starts the test after the participant presses the "Start Test" button.
       $("body").on("click", "#StartTestButton", function(event) {
            // For: Test Condition #3. Ensures the nature video is fully downloaded before allowing the user to begin the test.
            if((ov_test_condition === 3) && (!nature_video_download_complete)) {
                return;
            }
            
            // Same idea but for Test Condition #4 with the break reminder pop-up image.
            else if((ov_test_condition === 4) && (!break_reminder_download_complete)) {
                return;
            }
           
            // Hides the login welcome message.
            $("#h1_page_title").remove();
            $("#login_welcome_msg").remove();
            $("#StartTestButton").remove();

            var thirty_seconds_in_ms = parseFloat(30000);  // 30 seconds = 30,000 ms [Used between the practice and the real test.]
            var practice_time_in_ms = thirty_seconds_in_ms;  // Practice Test is only 30 seconds for now.
            var twelve_minutes_in_ms = parseFloat(720000);  // 12 minutes = 720,000 ms
            var four_minutes_in_ms =   parseFloat(240000);  //  4 minutes = 240,000 ms
            
            // Inaccurate... ALPHA TESTING VALUES.
            //var twelve_minutes_in_ms = parseFloat(15000);  // 15 seconds = 15,000 ms
            //var twelve_minutes_in_ms = parseFloat(120000);  // 2 minutes = 120,000 ms
            //var four_minutes_in_ms = thirty_seconds_in_ms;
            //var twelve_minutes_in_ms = parseFloat(360000);  // 6 minutes = 360,000 ms
            
            // Begins the 30-second practice test regardless of which condition is used.
            showVigilanceDisplay("Practice Test");
            
            // Custom break message that will be used as the `extra_text' argument for the hideVigilanceDisplay() function
            // that is shown to the user in-between the practice and the actual tests.
            var descriptive_text_between_practice_and_real_tests = 
                    "The test will automatically begin shortly. Please be ready.<br />" +
                    "Again, press the <strong>space bar</strong> only when you see the letter " +
                    "<strong>O</strong>, not for any other letter.";
            
            // Stops the practice test and gives the user a 30-second mini-break.
            setTimeout(hideVigilanceDisplay, practice_time_in_ms, "Test Begins In: <span id=\"ov_timer_minutes\"></span><span id=\"ov_timer_minutes_label\"></span><span id=\"ov_timer_seconds\">30</span> seconds", descriptive_text_between_practice_and_real_tests);
            setTimeout(countdownTimer, practice_time_in_ms, 30);
            
            // Starts the real test after the 30-second mini-break.
            setTimeout(showVigilanceDisplay, (practice_time_in_ms + thirty_seconds_in_ms));
            
            // -----------------------------------------------------------------------------------------------------------------
            // The `ov_test_condition' var was set via PHP, which changes depending on which condition the user is assigned to.
            // 
            // Test Condition #1: No break. Two back-to-back 12 minute task blocks.
            // Test Condition #2: 12 minute task. Break w/a blank, white screen. 12 minute task again.
            // Test Condition #3: 12 minute task. Break w/a nature video. 12 minute task again.
            // Test Condition #4: 12 minute task. Break w/random letters still flashing. 12 minute task again.
            // ----------------------------------------------------------------------------------------------------------------- 
            if(ov_test_condition === 1) {
                // No break for the participant. Concludes the test once 24 minutes have passed.
                setTimeout(concludeTest, (practice_time_in_ms + thirty_seconds_in_ms + twelve_minutes_in_ms * 2));
            } 
            
            else if(ov_test_condition === 2) {
                // Hides the initial OV display after 12 minutes to show a blank screen.
                setTimeout(hideVigilanceDisplay, (practice_time_in_ms + thirty_seconds_in_ms + twelve_minutes_in_ms), "Break: <span id=\"ov_timer_minutes\">4</span><span id=\"ov_timer_minutes_label\"> minutes and </span><span id=\"ov_timer_seconds\">0</span> seconds");
                setTimeout(countdownTimer,       (practice_time_in_ms + thirty_seconds_in_ms + twelve_minutes_in_ms), 240);  // 240 seconds = 4 minutes
                
                // Shows the OV display again after the 4 minute break.
                setTimeout(showVigilanceDisplay, (practice_time_in_ms + thirty_seconds_in_ms + twelve_minutes_in_ms + four_minutes_in_ms));
                
                // Concludes the test once 28 minutes have passed (12 min + 4 min + 12 min).
                setTimeout(concludeTest, (practice_time_in_ms + thirty_seconds_in_ms + twelve_minutes_in_ms * 2 + four_minutes_in_ms));
            }
            
            else if(ov_test_condition === 3) {
                // Hides the initial OV display after 12 minutes to show a nature video.
                setTimeout(hideVigilanceDisplay, (practice_time_in_ms + thirty_seconds_in_ms + twelve_minutes_in_ms), "Break: <span id=\"ov_timer_minutes\">4</span><span id=\"ov_timer_minutes_label\"> minutes and </span><span id=\"ov_timer_seconds\">0</span> seconds", "", true);
                setTimeout(countdownTimer,       (practice_time_in_ms + thirty_seconds_in_ms + twelve_minutes_in_ms), 240);  // 240 seconds = 4 minutes
                
                // Shows the OV display again after the 4 minute break.
                setTimeout(showVigilanceDisplay, (practice_time_in_ms + thirty_seconds_in_ms + twelve_minutes_in_ms + four_minutes_in_ms), "", true);
                
                // Concludes the test once 28 minutes have passed (12 min + 4 min + 12 min).
                setTimeout(concludeTest, (practice_time_in_ms + thirty_seconds_in_ms + twelve_minutes_in_ms * 2 + four_minutes_in_ms));
            }
            
            else {
                // Test Condition #4
                // Hides the initial OV display after 12 minutes, and then shows another iteration of it during the Break.
                // Although, this `showVigilanceDisplay()' will NOT record the user's activity.
                setTimeout(showVigilanceDisplay, (practice_time_in_ms + thirty_seconds_in_ms + twelve_minutes_in_ms), "Break: <span id=\"ov_timer_minutes\">4</span><span id=\"ov_timer_minutes_label\"> minutes and </span><span id=\"ov_timer_seconds\">0</span> seconds");
                setTimeout(countdownTimer,       (practice_time_in_ms + thirty_seconds_in_ms + twelve_minutes_in_ms), 240);  // 240 seconds = 4 minutes
                
                // Shows the OV display again after the 4 minute break.
                setTimeout(showVigilanceDisplay, (practice_time_in_ms + thirty_seconds_in_ms + twelve_minutes_in_ms + four_minutes_in_ms));
                
                // Concludes the test once 28 minutes have passed (12 min + 4 min + 12 min).
                setTimeout(concludeTest, (practice_time_in_ms + thirty_seconds_in_ms + twelve_minutes_in_ms * 2 + four_minutes_in_ms));
            }
       });
    });
    
})(window, jQuery);

// @license-end
