<?php
/* --------------------------------------------------------------------------------------------------------------
 * Copyright (C) 2018 by William Paul Liggett (junktext@junktext.com)
 * This Source Code Form is subject to the terms of the Mozilla Public License (MPL), v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Filename:  output_json_response.php
 * 
 * Purpose:   Function to standardise the output of various Ajax requests using JSON as the data storage. 
 * -------------------------------------------------------------------------------------------------------------- */

function output_json_response($successful = false, $error_message = "Incorrect usage.", $extra_data1 = "", $extra_data2 = "") {
    // Ensures the variables must be set correctly.
    if(!isset($successful) || !is_bool($successful)) {
        $successful = false;
        $error_message = "output_json_response($successful, $error_message, $extra_data): 1st argument, $successful, must be a boolean value.";
    }
    
    if(!isset($error_message) || !is_string($error_message)) {
        $successful = false;
        $error_message = "output_json_response($successful, $error_message, $extra_data): 2nd argument, $error_message, must be a string.";
    }
    
    // Gets rid of potentially harmful injection characters.
    $error_message = htmlspecialchars($error_message, ENT_QUOTES, 'UTF-8');
    $extra_data1 = htmlspecialchars($extra_data1, ENT_QUOTES, 'UTF-8');
    $extra_data2 = htmlspecialchars($extra_data2, ENT_QUOTES, 'UTF-8');
    
    // JSON array to output as a web service.
    $json_data = [
        'successful' => $successful,
        'error_message' => $error_message,
        'extra_data1' => $extra_data1,
        'extra_data2' => $extra_data2,
    ];
    
    echo json_encode($json_data);
    return;
}
