<?php
/* --------------------------------------------------------------------------------------------------------------
 * Copyright (C) 2018 by William Paul Liggett (junktext@junktext.com)
 * This Source Code Form is subject to the terms of the Mozilla Public License (MPL), v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Filename:  scheduled_test_edit.php
 * 
 * Purpose:   Modify an existing OpenVigilance Task test that was previously scheduled in the database.
 *            For example, the subject's ID or the test condition might need to be changed. 
 *            The page is meant to be used as a simple web service, such as with Ajax or the like.
 * 
 * Output:    JSON array as defined in ``output_json_response.php''.
 * -------------------------------------------------------------------------------------------------------------- */

// Maintains the authenticated user session across different admin pages.
// Ensures only valid administrators can do anything.
session_start();

// Function: output_json_response($successful, $error_message)
require_once "output_json_response.php";

if(!isset($_SESSION['ov_admin_user'])) {
    output_json_response(false, "Not logged on as an administrator.");
    return;
}

// ------------------------------------------------------------------------------------
// POST input variables sent by the web browser.
// Note: The login_code cannot be modified by an OV admin. The code is auto-generated.
// ------------------------------------------------------------------------------------
$sk = intval($_POST['edit_test_sk']);  // Surrogate Key identifier of the scheduled test in the database.
$subject_id = intval($_POST['edit_test_subject_id']);  // The ID of the person meant to take the test.
$test_condition = intval($_POST['edit_test_condition']);  // The kind of test being taken.

// Validate the data.
if($subject_id <= 0 || $test_condition <= 0) {
    output_json_response(false, "The subject_id nor the test_condition cannot be less than or equal to zero.");
    return;
}

else if($test_condition > 4) {
    output_json_response(false, "The test_condition cannot be greater than 4.");
    return;
}

else if($sk <= 0) {
    output_json_response(false, "Invalid surrogate key (sk).");
    return;
}

// Logs into the OpenVigilance Task tests database to control and alter user tests.
// `$pdo' is defined as the database connection.
require_once "../settings.php";
require_once "../" . PROTECTED_SITE_CONFIGS_DIR . "openvigilance_db_connection_admin.php";

// Uses MySQL transactions to be atomic.
// MySQL Command: START TRANSACTION;
$pdo->beginTransaction();

// Modifies the record in the database to match the OV admin's edit request.
$sql = "UPDATE test_control SET subject_id=:subject_id, test_condition=:test_condition WHERE sk=:sk";
$statement = $pdo->prepare($sql);
$statement->bindValue(":subject_id", $subject_id, PDO::PARAM_INT);
$statement->bindValue(":test_condition", $test_condition, PDO::PARAM_INT);
$statement->bindValue(":sk", $sk, PDO::PARAM_INT);
$db_updated = $statement->execute();  // true if the SQL was executed successfully

if($db_updated) {
    // MySQL Transaction: COMMIT;
    $pdo->commit();
}

else {
    // MySQL Transaction: ROLLBACK;
    $pdo->rollBack();

    output_json_response(false, "The input data was valid, but the database could not be updated at this time for some reason.");
    return;
}

/* !@#$ ... I MIGHT TRY TO FIGURE OUT HOW TO RESOLVE THIS LATER.
 * 
// Ensures that two tests cannot be scheduled for the same subject at the same time.
// In other words, the subject_id cannot have two tests in the database with the value `test_scheduled=1'.
$sql = "SELECT COUNT(*) FROM test_control WHERE subject_id=:subject_id AND test_scheduled=2";
$statement = $pdo->prepare($sql);
$statement->bindValue(":subject_id", $subject_id, PDO::PARAM_INT);
$statement->execute();
$num_rows = $statement->fetch()[0];

if(intval($num_rows) === 2) {
    // Deletes the record we just apparently added by mistake.
    $sql = "DELETE FROM test_control WHERE sk=:sk";
    $statement = $pdo->prepare($sql);
    $statement->bindValue(":subject_id", $subject_id, PDO::PARAM_INT);
    $statement->bindValue(":test_condition", $test_condition, PDO::PARAM_INT);
    $statement->bindValue(":sk", $sk, PDO::PARAM_INT);
    
    output_json_response(false, "The subject_id already has a test scheduled. Please edit or remove that pre-existing test instead.");
    return;
}
 *
 */

// Verify the database has updated the test record.
$sql = "SELECT subject_id, test_condition FROM test_control WHERE sk=:sk";
$statement = $pdo->prepare($sql);
$statement->bindValue(":sk", $sk, PDO::PARAM_INT);
$statement->execute();
$modified_record = $statement->fetch();

// `$modified_record' should now be an array of one row with two named indexes: ['subject_id'] and ['test_condition']
// Initially, the all cell contents of the `$modified_record' are strings when grabbing data from MySQL. This is why intval() is used.
// This behavior can be overridden to grab the native MySQL database types, but it's not that essential right now.
// See: https://stackoverflow.com/questions/34397994/pdo-fetch-data-returns-array-of-string
if((intval($modified_record['subject_id']) === $subject_id) && (intval($modified_record['test_condition']) === $test_condition)) {
    // Inform the web browser that all went well.
    output_json_response(true, "");    
    return;
}

else {
    // The database was not properly updated for some reason.
    output_json_response(false, "The input data was valid, but the database could not be updated at this time for some reason.");
    return;
}
