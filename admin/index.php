<?php
    // Enables user sessions.
    session_start();
    
    // Force the user to connect via "https://".
    if($_SERVER["HTTPS"] != "on") {
        header("Location: https://" . $_SERVER["HTTP_HOST"] . htmlspecialchars($_SERVER["REQUEST_URI"], ENT_QUOTES, "UTF-8"));
        exit();
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>OpenVigilance Task: Admin Page</title>
        <!-- Copyright (C) 2018 by William Paul Liggett (junktext@junktext.com)
             This Source Code Form is subject to the terms of the Mozilla Public License (MPL), v. 2.0.
             If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/. -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Administrative login to control various settings of the OpenVigilance Task tests." />
        <link rel="stylesheet" type="text/css" href="../css/style.css" />
        <link rel="stylesheet" href="../js/jquery-ui-themes-1.12.1/themes/base/jquery-ui.css" />
    </head>
    <body>
        <h1>OpenVigilance Task: Admin Page</h1>
        <?php
            // Makes the tab whitespace makes the HTML source look nice. There are 4 spaces.
            $tab = "    ";
        
            // Displays if any login errors were detected. No error = Non-breaking space character. 
            // HTML needs something besides an empty string to display the element to the DOM.
            $error_msg = "&nbsp;";
        
            // Used to display an HTML login form on first load or if the username and password was incorrect.
            // The <form action="." just simply means it sends the request to this page (index.php) in the current directory.
            $login_form = PHP_EOL .
                          "$tab$tab<form action=\".\" name=\"AdminLogin\" id=\"AdminLogin\" method=\"post\">" . PHP_EOL .
                          "$tab$tab$tab<label for=\"username\">Username:</label>" . PHP_EOL .
                          "$tab$tab$tab<input type=\"text\" name=\"username\" id=\"username\" />" . PHP_EOL .
                          "$tab$tab$tab<br />" . PHP_EOL .
                          "$tab$tab$tab<label for=\"password\">Password:</label>" . PHP_EOL .
                          "$tab$tab$tab<input type=\"password\" name=\"password\" id=\"password\" />" . PHP_EOL .
                          "$tab$tab$tab<br />" . PHP_EOL .
                          "$tab$tab$tab<input type=\"submit\" value=\"Submit\" />" . PHP_EOL .
                          "$tab$tab</form>" . PHP_EOL;
            
            // GET: The page upon first load, before the <form> has been submitted.
            if($_SERVER['REQUEST_METHOD'] === "GET") {
                // Displays the HTML login <form>.
                // There should be no error on first load, but this keeps the form layout in the same position.
                echo "<p class='error_msg'>$error_msg</p>";
                echo $login_form;
            }
        
            // POST: Confirms if the administrative password is accurate after the <form> has been submitted.
            // Note: I am specifically defining an `else if' clause since REQUEST_METHOD could be: GET, POST, HEAD, or PUT
            else if($_SERVER['REQUEST_METHOD'] === "POST") {
                // Grabs what the user submitted and sanitizes the input to avoid an SQL injection or XSS attack.
                $submitted_username = htmlspecialchars($_POST['username'], ENT_QUOTES, 'UTF-8');
                $submitted_password = htmlspecialchars($_POST['password'], ENT_QUOTES, 'UTF-8');
                
                // Confirms whether the login is valid and sets the boolean `$valid_login_admin' variable.
                require_once "valid_login_admin.php";
                
                // Login invalid: Re-displays the login <form>.
                if(!$valid_login_admin) {
                    echo "<p class='error_msg'>$error_msg</p>";
                    echo $login_form;
                }
             
                // Login successful!
                else {
                    // Maintains the authenticated user session across different admin pages.
                    if(!isset($_SESSION['ov_admin_user'])) {
                        $_SESSION['ov_admin_user'] = $submitted_username;
                    }
                    
                    // Logs into the OpenVigilance Task tests database to control and alter user tests.
                    // `$pdo' is defined as the database connection.
                    require_once "../settings.php";
                    require_once "../" . PROTECTED_SITE_CONFIGS_DIR . "openvigilance_db_connection_admin.php";
                    
                    // Test Control: Active tests.
                    $sql = "SELECT sk, subject_id, test_condition, login_code FROM test_control WHERE test_scheduled=1 ORDER BY subject_id";
                    $statement = $pdo->prepare($sql);
                    $statement->execute();
                    $rows = $statement->fetchAll();  // Grabs all of the rows.
                    
                    // Section heading.
                    echo "<p class=\"center\" style=\"font-size: 11pt;\"><strong>Captured User Test Data:</strong> <a href=\"../user-data-files\" target=\"_blank\">CSV (Comma-Separated Values) Files</a><br />" . PHP_EOL;
                    echo "Tip: Removing a scheduled test will <em>not</em> delete any previously-recorded CSV data files.</p>" . PHP_EOL;
                    echo "<h2>Scheduled Tests</h2>" . PHP_EOL;
                    
                    // Creates the main <form> to enable the add, edit, and remove button functionalities.
                    echo "$tab$tab<form action=\".\" name=\"ScheduledTests\" method=\"post\">" . PHP_EOL;

                    // Sets up the <table> heading.
                    echo "$tab$tab$tab<table name=\"table_scheduled_tests\" id=\"table_scheduled_tests\">" . PHP_EOL .
                         "$tab$tab$tab$tab<thead>" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<tr>" . PHP_EOL .
                         "$tab$tab$tab$tab$tab$tab<th>&nbsp;</th>" . PHP_EOL .
                         "$tab$tab$tab$tab$tab$tab<th>Subject ID</th>" . PHP_EOL .
                         "$tab$tab$tab$tab$tab$tab<th>Test Condition</th>" . PHP_EOL .
                         "$tab$tab$tab$tab$tab$tab<th>Login Code</th>" . PHP_EOL .
                         "$tab$tab$tab$tab$tab</tr>" . PHP_EOL .
                         "$tab$tab$tab$tab</thead>" . PHP_EOL .
                         "$tab$tab$tab$tab<tbody>" . PHP_EOL;

                    // The rows of the <table>.
                    foreach($rows as $key => $value) {
                        echo "$tab$tab$tab$tab$tab<tr>" . PHP_EOL .
                             // I'm putting the "data-ov-..." values in the radio button element to make the JavaScript references less annoying.
                             "$tab$tab$tab$tab$tab$tab<td><input type=\"radio\" name=\"scheduled_test\" " .
                                                      "id=\"" . $value["sk"] . "\" value=\"" . $value["sk"] . "\" " .
                                                      "data-ov-subject-id=\"" . $value["subject_id"] . "\" " .
                                                      "data-ov-test-condition=\"" . $value["test_condition"] . "\" " .
                                                      "data-ov-login-code=\"" . $value["login_code"] . "\" /></td>" . PHP_EOL .
                             "$tab$tab$tab$tab$tab$tab<td>" . $value["subject_id"] . "</td>" . PHP_EOL .
                             "$tab$tab$tab$tab$tab$tab<td>" . $value["test_condition"] . "</td>" . PHP_EOL .
                             "$tab$tab$tab$tab$tab$tab<td>" . $value["login_code"] . "</td>" . PHP_EOL .
                             "$tab$tab$tab$tab$tab</tr>" . PHP_EOL;
                    }
                    
                    // Closes out the <table>
                    echo "$tab$tab$tab$tab</tbody>" . PHP_EOL .
                         "$tab$tab$tab</table>" . PHP_EOL;

                    // Closes out the main <form>.
                    echo "$tab$tab$tab<div id=\"scheduled_tests_buttons\">" . PHP_EOL .
                         "$tab$tab$tab$tab<input type=\"button\" name=\"add_test\" id=\"add_test\" value=\"Add Test\" />" . PHP_EOL .  
                         "$tab$tab$tab$tab<input type=\"button\" name=\"edit_test\" id=\"edit_test\" value=\"Edit Test\" />" . PHP_EOL .
                         "$tab$tab$tab$tab<input type=\"button\" name=\"remove_test\" id=\"remove_test\" value=\"Remove Test\" />" . PHP_EOL .     
                         "$tab$tab$tab</div>" . PHP_EOL .
                         "$tab$tab</form>" . PHP_EOL;

                    // ---------------------------------------------------------------------------------------------------------
                    // Add Test: Pop-up Dialog Form. Uses jQuery UI to pop-out when needed.
                    echo "$tab$tab<div id=\"dialog_form_add_test\" title=\"Add Test\" class=\"hidden\">" . PHP_EOL .
                         "$tab$tab$tab<p class=\"validateTips\">All form fields are required.</p>" . PHP_EOL .
                         "$tab$tab$tab<form id=\"AddTest\">" . PHP_EOL .
                         "$tab$tab$tab$tab<fieldset>" . PHP_EOL .
                         //"$tab$tab$tab$tab$tab<input type=\"hidden\" name=\"add_test_sk\" id=\"add_test_sk\" value=\"\" />" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<label for=\"add_test_subject_id\">Subject ID</label>" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<input type=\"text\" name=\"add_test_subject_id\" id=\"add_test_subject_id\" value=\"\" class=\"text ui-widget-content ui-corner-all\" />" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<label for=\"add_test_condition\">Test Condition</label>" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<input type=\"text\" name=\"add_test_condition\" id=\"add_test_condition\" value=\"\" class=\"text ui-widget-content ui-corner-all\" />" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<label for=\"add_login_code\">Login Code</label>" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<input type=\"text\" name=\"add_login_code\" id=\"add_login_code\" value=\"Auto-generated.\" disabled=\"disabled\" title=\"Cannot edit as the code is auto-generated.\" class=\"text ui-widget-content ui-corner-all\" />" . PHP_EOL .

                         // Allow form submission with keyboard without duplicating the dialog button
                         "$tab$tab$tab$tab$tab<input type=\"submit\" tabindex=\"-1\" style=\"position:absolute; top:-1000px\">" . PHP_EOL .
                         "$tab$tab$tab$tab</fieldset>" . PHP_EOL .
                         "$tab$tab$tab</form>" . PHP_EOL .
                         "$tab$tab</div>" . PHP_EOL;

                    // Edit Test: Pop-up Dialog Form. Uses jQuery UI to pop-out when needed.
                    echo "$tab$tab<div id=\"dialog_form_edit_test\" title=\"Edit Test\" class=\"hidden\">" . PHP_EOL .
                         "$tab$tab$tab<p class=\"validateTips\">All form fields are required.</p>" . PHP_EOL .
                         "$tab$tab$tab<form id=\"EditTest\">" . PHP_EOL .
                         "$tab$tab$tab$tab<fieldset>" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<input type=\"hidden\" name=\"edit_test_sk\" id=\"edit_test_sk\" value=\"\" />" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<label for=\"edit_test_subject_id\">Subject ID</label>" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<input type=\"text\" name=\"edit_test_subject_id\" id=\"edit_test_subject_id\" value=\"\" class=\"text ui-widget-content ui-corner-all\" />" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<label for=\"edit_test_condition\">Test Condition</label>" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<input type=\"text\" name=\"edit_test_condition\" id=\"edit_test_condition\" value=\"\" class=\"text ui-widget-content ui-corner-all\" />" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<label for=\"edit_login_code\">Login Code</label>" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<input type=\"text\" name=\"edit_login_code\" id=\"edit_login_code\" value=\"\" disabled=\"disabled\" title=\"Cannot edit as the code is auto-generated.\" class=\"text ui-widget-content ui-corner-all\" />" . PHP_EOL .

                         // Allow form submission with keyboard without duplicating the dialog button
                         "$tab$tab$tab$tab$tab<input type=\"submit\" tabindex=\"-1\" style=\"position:absolute; top:-1000px\">" . PHP_EOL .
                         "$tab$tab$tab$tab</fieldset>" . PHP_EOL .
                         "$tab$tab$tab</form>" . PHP_EOL .
                         "$tab$tab</div>" . PHP_EOL;

                    // Remove Test: Pop-up Dialog Form. Uses jQuery UI to pop-out when needed.
                    // There are extra hidden elements as diabled input text box fields cannot be submitted to the server per the W3C.
                    echo "$tab$tab<div id=\"dialog_form_remove_test\" title=\"Remove Test\" class=\"hidden\">" . PHP_EOL .
                         "$tab$tab$tab<p>Do you want to remove this test?</p>" . PHP_EOL .
                         "$tab$tab$tab<form id=\"RemoveTest\">" . PHP_EOL .
                         "$tab$tab$tab$tab<fieldset>" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<input type=\"hidden\" name=\"remove_test_sk\" id=\"remove_test_sk\" value=\"\" />" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<label for=\"remove_test_subject_id\">Subject ID</label>" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<input type=\"text\" name=\"remove_test_subject_id\" id=\"remove_test_subject_id\" value=\"\" disabled=\"disabled\" class=\"text ui-widget-content ui-corner-all\" />" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<input type=\"hidden\" name=\"remove_test_subject_id_hidden\" id=\"remove_test_subject_id_hidden\" value=\"\" />" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<label for=\"remove_test_condition\">Test Condition</label>" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<input type=\"text\" name=\"remove_test_condition\" id=\"remove_test_condition\" value=\"\" disabled=\"disabled\" class=\"text ui-widget-content ui-corner-all\" />" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<input type=\"hidden\" name=\"remove_test_condition_hidden\" id=\"remove_test_condition_hidden\" value=\"\" />" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<label for=\"remove_login_code\">Login Code</label>" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<input type=\"text\" name=\"remove_login_code\" id=\"remove_login_code\" value=\"\" disabled=\"disabled\" class=\"text ui-widget-content ui-corner-all\" />" . PHP_EOL .
                         "$tab$tab$tab$tab$tab<input type=\"hidden\" name=\"remove_login_code_hidden\" id=\"remove_login_code_hidden\" value=\"\" />" . PHP_EOL .

                         // Allow form submission with keyboard without duplicating the dialog button
                         "$tab$tab$tab$tab$tab<input type=\"submit\" tabindex=\"-1\" style=\"position:absolute; top:-1000px\">" . PHP_EOL .
                         "$tab$tab$tab$tab</fieldset>" . PHP_EOL .
                         "$tab$tab$tab</form>" . PHP_EOL .
                         "$tab$tab</div>" . PHP_EOL;

                    // Error Pop-up Dialog: Cannot edit/remove a test that hasn't been selected.
                    echo "$tab$tab<div id=\"dialog_form_error_message\" title=\"Error\" class=\"hidden\">" . PHP_EOL .
                         "$tab$tab$tab<p>Please select a test that you want to <span id=\"error_edit_or_remove_span\"></span>.</p>" . PHP_EOL .
                         "$tab$tab</div>" . PHP_EOL;
                    
                    // There were no rows to display. More OV tests should be added.
                    //else {
                        //echo '<p class="center">There are no tests currently scheduled.</p>';
                    //}
                }
            }
        ?>
        <script src="../js/jquery-3.3.1.min.js"></script>
        <script src="../js/jquery-ui-1.12.1/jquery-ui.js"></script>
        <script src="../js/openvigilance-admin.js"></script>
    </body>
</html>
