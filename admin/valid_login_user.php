<?php
/* --------------------------------------------------------------------------------------------------------------
 * Copyright (C) 2018 by William Paul Liggett (junktext@junktext.com)
 * This Source Code Form is subject to the terms of the Mozilla Public License (MPL), v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Filename:  validate_login_user.php
 * 
 * Purpose:   Ensure that only people with a scheduled OV test can take the test.
 * -------------------------------------------------------------------------------------------------------------- */

$valid_login_user = false;

// ************************************************************************************************************
// *** Ensure that the calling PHP script has the following `require_once' directive.
// *** Also, the number of "../" may need to be more or less depending on where the calling script is located.
// ************************************************************************************************************
// Logs into the OpenVigilance Task tests database to verify the person is allowed to take a test.
// `$pdo' is defined as the database connection.
// require_once "settings.php";
// require_once "PROTECTED_SITE_CONFIGS_DIR . "openvigilance_db_connection_user.php";

// Confirms the person's temporary account is valid.
// The `$submitted_username' and `$submitted_password' variables are from the calling PHP script.
$sql = "SELECT sk, subject_id, test_condition, login_code FROM test_control WHERE test_scheduled=1 AND subject_id=:subject_id AND login_code=:login_code";
$statement = $pdo->prepare($sql);
$statement->bindValue(":subject_id", $submitted_username, PDO::PARAM_INT);  // Regular users only have numeric account names.
$statement->bindValue(":login_code", $submitted_password, PDO::PARAM_STR);
$statement->execute();
$account_found = $statement->fetch();  // If successful, grabs the stored login details from the database. 'false' if not.

// Determines if the login was valid or not.
// The `$error_msg' is a standardized variable in all calling scripts to output the login error found.
if(!$account_found) {
    $error_msg = "Error: The username or password is not valid on this system.<br />Note: A user account is disabled after completing a vigilance test.";
    return;
}

// Valid login!
else {
    $valid_login_user = true;
}
