// @license magnet:?xt=urn:btih:3877d6d54b3accd4bc32f8a48bf32ebc0901502a&dn=mpl-2.0.txt MPL-2.0

/* ------------------------------------------------------------------------------------------------------------------ 
    Copyright (C) 2018 by William Paul Liggett (junktext@junktext.com)
    This Source Code Form is subject to the terms of the Mozilla Public License (MPL), v. 2.0.
    If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.    
------------------------------------------------------------------------------------------------------------------ */

// GLOBAL VARS: Keeps track of which row (scheduled OV test) the admin is adding, editing, or removing.
        var DOMelem_radio_btn_selected;
        
        var scheduled_sk;
        var scheduled_subject_id;
        var scheduled_test_condition;
        var scheduled_login_code;
        
        var new_scheduled_subject_id;
        var new_scheduled_test_condition;

;(function(window, $) {
    $(document).ready(function() {
        var tips = $(".validateTips");

        function updateTips(t) {
            tips
                .text(t)
                .addClass("ui-state-highlight");
            setTimeout(function() {
                  tips.removeClass("ui-state-highlight", 1500);
            }, 500);
        }

        function checkLength(o, n, min, max) {
            if (o.val().length > max || o.val().length < min) {
                o.addClass("ui-state-error");
                updateTips("Length of " + n + " must be between " + min + " and " + max + ".");
                return false;
            } else {
                return true;
            }
        }

        function checkRegexp(o, regexp, n) {
            if (!(regexp.test(o.val()))) {
                o.addClass("ui-state-error");
                updateTips(n);
                return false;
            } else {
                return true;
            }
        }
        
        // -------------------------------------------------------------------------------------------------
        //                                            Add Test
        // -------------------------------------------------------------------------------------------------
        var add_dialog, add_form,
            add_subject_id = $("#add_test_subject_id"),
            add_test_condition = $("#add_test_condition"),
            add_login_code = $("#add_login_code"),
            add_allFields = $([]).add(add_subject_id).add(add_test_condition).add(add_login_code);

        function addTest() {
            var valid = true;
            add_allFields.removeClass("ui-state-error");

            /*
            valid = valid && checkLength(name, "username", 3, 16);
            valid = valid && checkLength(email, "email", 6, 80);
            valid = valid && checkLength(password, "password", 5, 16);

            valid = valid && checkRegexp(name, /^[a-z]([0-9a-z_\s])+$/i, "Username may consist of a-z, 0-9, underscores, spaces and must begin with a letter.");
            valid = valid && checkRegexp(email, emailRegex, "eg. ui@jquery.com");
            valid = valid && checkRegexp(password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9");

            if (valid) {
                $("#users tbody").append("<tr>" +
                    "<td>" + name.val() + "</td>" +
                    "<td>" + email.val() + "</td>" +
                    "<td>" + password.val() + "</td>" +
                "</tr>");
                dialog.dialog("close");
            }
            */
           
           if(valid) {
               new_scheduled_subject_id = $("#add_test_subject_id").val();
               new_scheduled_test_condition = $("#add_test_condition").val();
               
               // The data sent to the server is what the admin wanted modified via the pop-up dialog defined further below.
               // Specifically, see the Getters and Setters near the bottom.
               $.ajax({
                   type:        'POST',
                   url:         "../admin/scheduled_test_add.php",
                   dataType:    "json",
                   data:        $("form#AddTest").serialize(),
                   success:     function(response) {
                                    // Success! Updates the DOM by modifying the rows in the <table>.
                                    if(response.successful === true) {
                                        // New surrogate key (sk) for the recently added row.
                                        var new_scheduled_login_code = response.extra_data1;
                                        var new_scheduled_sk = response.extra_data2;
                                        
                                        $("#table_scheduled_tests tbody").append('' +
                                            '<tr>' +
                                                '<td><input type="radio" name="scheduled_test" id="' + new_scheduled_sk + '" value="' + new_scheduled_sk + '" data-ov-subject-id="' + new_scheduled_subject_id + '" data-ov-test-condition="' + new_scheduled_test_condition + '" data-ov-login-code="' + new_scheduled_login_code + '" /></td>' +
                                                '<td>' + new_scheduled_subject_id + '</td>' +
                                                '<td>' + new_scheduled_test_condition + '</td>' +
                                                '<td>' + new_scheduled_login_code + '</td>' +
                                            '</tr>');
                           
                                        // Places the new test in the correct order (ascending) by subject_id into the <table>.
                                        //
                                    }
                                    
                                    // Error
                                    else {
                                        alert("Error: " + response.error_message);
                                    }
                                }
               });
               
               // Auto-closes the pop-up add dialog for the user.
               add_dialog.dialog("close");
           }
            return valid;
        }

        add_dialog = $("#dialog_form_add_test").dialog({
            autoOpen: false,
            height: 400,
            width: 350,
            modal: true,
            buttons: {
                "Submit": addTest,
                Cancel: function() {
                    add_dialog.dialog("close");
                }
            },
            close: function() {
                document.getElementById("AddTest").reset();
                add_allFields.removeClass("ui-state-error");
            }
        });

        add_form = add_dialog.find("form#dialog_form_add_test").on("submit", function(event) {
            event.preventDefault();
            addTest();
        });

        // The "Add Test" button on the main <form> that causes the pop-up dialog to show.
        $("#add_test").button().on("click", function() {
            // Opens the pop-up add dialog for the user.
            add_dialog.dialog("open");
        });
        
        
        // -------------------------------------------------------------------------------------------------
        //                                           Edit Test
        // -------------------------------------------------------------------------------------------------
        var edit_dialog, edit_form,
            edit_subject_id = $("#edit_test_subject_id"),
            edit_test_condition = $("#edit_test_condition"),
            edit_login_code = $("#edit_login_code"),
            edit_allFields = $([]).add(edit_subject_id).add(edit_test_condition).add(edit_login_code);

        function editTest() {
            var valid = true;
            edit_allFields.removeClass("ui-state-error");

            /*
            valid = valid && checkLength(name, "username", 3, 16);
            valid = valid && checkLength(email, "email", 6, 80);
            valid = valid && checkLength(password, "password", 5, 16);

            valid = valid && checkRegexp(name, /^[a-z]([0-9a-z_\s])+$/i, "Username may consist of a-z, 0-9, underscores, spaces and must begin with a letter.");
            valid = valid && checkRegexp(email, emailRegex, "eg. ui@jquery.com");
            valid = valid && checkRegexp(password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9");

            if (valid) {
                $("#users tbody").append("<tr>" +
                    "<td>" + name.val() + "</td>" +
                    "<td>" + email.val() + "</td>" +
                    "<td>" + password.val() + "</td>" +
                "</tr>");
                dialog.dialog("close");
            }
            */
           
           if(valid) {
               new_scheduled_subject_id = $("#edit_test_subject_id").val();
               new_scheduled_test_condition = $("#edit_test_condition").val();
               
               // The data sent to the server is what the admin wanted modified via the pop-up dialog defined further below.
               // Specifically, see the Getters and Setters near the bottom.
               $.ajax({
                   type:        'POST',
                   url:         "../admin/scheduled_test_edit.php",
                   dataType:    "json",
                   data:        $("form#EditTest").serialize(),
                   success:     function(response) {
                                    // Success! Updates the DOM by modifying the rows in the <table>.
                                    if(response.successful === true) {
                                        // Updates the meta-data
                                        DOMelem_radio_btn_selected.attr("data-ov-subject-id", new_scheduled_subject_id);
                                        DOMelem_radio_btn_selected.attr("data-ov-test-condition", new_scheduled_test_condition);
                                        
                                        // Updates the visual diplay on the <td> columns.
                                        DOMelem_radio_btn_selected.parent().next().html(new_scheduled_subject_id);
                                        DOMelem_radio_btn_selected.parent().next().next().html(new_scheduled_test_condition);
                                    }
                                    
                                    // Error
                                    else {
                                        alert("Error: " + response.error_message);
                                    }
                                }
               });
               
               // Auto-closes the pop-up edit dialog for the user.
               edit_dialog.dialog("close");
           }
            return valid;
        }

        edit_dialog = $("#dialog_form_edit_test").dialog({
            autoOpen: false,
            height: 400,
            width: 350,
            modal: true,
            buttons: {
                "Submit Changes": editTest,
                Cancel: function() {
                    edit_dialog.dialog("close");
                }
            },
            close: function() {
                document.getElementById("EditTest").reset();
                edit_allFields.removeClass("ui-state-error");
            }
        });

        edit_form = edit_dialog.find("form#dialog_form_edit_test").on("submit", function(event) {
            event.preventDefault();
            editTest();
        });

        // The "Edit Test" button on the main <form> that causes the pop-up dialog to show.
        $("#edit_test").button().on("click", function() {
            // Error Dialog Message: Cannot edit a test that hasn't been selected.
            if(typeof($("input[type='radio']:checked").val()) === "undefined") {
                // Sets the text of the dialog to say "edit".
                $("#error_edit_or_remove_span").html("edit");
                
                // Error dialog configuration.
                $("#dialog_form_error_message").dialog({
                    resizable: false,
                    height: "auto",
                    width: 400,
                    modal: true,
                    buttons: {
                      Okay: function() {
                        $(this).dialog("close");
                      }
                    }
                });
            }
            
            // No error so far. Shows the pop-up dialog with the data filled in from the record that was selected by the user.
            else {
                // Getters. Grabs the record data of the row selected prior to any modifications by the edit dialog.
                DOMelem_radio_btn_selected = $("input[type='radio']:checked");
                scheduled_sk = DOMelem_radio_btn_selected.val();
                scheduled_subject_id = DOMelem_radio_btn_selected.attr("data-ov-subject-id");
                scheduled_test_condition = DOMelem_radio_btn_selected.attr("data-ov-test-condition");
                scheduled_login_code = DOMelem_radio_btn_selected.attr("data-ov-login-code");
                
                // Setters. Puts the data into the pop-up dialog for the user to edit.
                // Note: The 'sk' field isn't shown, but we need the data to be sent to the server nonetheless.
                $("#edit_test_sk").val(scheduled_sk);
                $("#edit_test_subject_id").val(scheduled_subject_id);
                $("#edit_test_condition").val(scheduled_test_condition);
                $("#edit_login_code").val(scheduled_login_code);
                
                // Opens the pop-up edit dialog for the user.
                edit_dialog.dialog("open");
            }
        });
        
        // -------------------------------------------------------------------------------------------------
        //                                           Remove Test
        // -------------------------------------------------------------------------------------------------
        var remove_dialog, remove_form,
            remove_subject_id = $("#remove_test_subject_id"),
            remove_test_condition = $("#remove_test_condition"),
            remove_login_code = $("#remove_login_code"),
            remove_allFields = $([]).add(remove_subject_id).add(remove_test_condition).add(remove_login_code);

        function removeTest() {
            var valid = true;
            remove_allFields.removeClass("ui-state-error");

            /*
            valid = valid && checkLength(name, "username", 3, 16);
            valid = valid && checkLength(email, "email", 6, 80);
            valid = valid && checkLength(password, "password", 5, 16);

            valid = valid && checkRegexp(name, /^[a-z]([0-9a-z_\s])+$/i, "Username may consist of a-z, 0-9, underscores, spaces and must begin with a letter.");
            valid = valid && checkRegexp(email, emailRegex, "eg. ui@jquery.com");
            valid = valid && checkRegexp(password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9");

            if (valid) {
                $("#users tbody").append("<tr>" +
                    "<td>" + name.val() + "</td>" +
                    "<td>" + email.val() + "</td>" +
                    "<td>" + password.val() + "</td>" +
                "</tr>");
                dialog.dialog("close");
            }
            */
           
           if(valid) {
               new_scheduled_subject_id = $("#remove_test_subject_id").val();
               new_scheduled_test_condition = $("#remove_test_condition").val();
               
               // The data sent to the server is what the admin wanted modified via the pop-up dialog defined further below.
               // Specifically, see the Getters and Setters near the bottom.
               $.ajax({
                   type:        'POST',
                   url:         "../admin/scheduled_test_remove.php",
                   dataType:    "json",
                   data:        $("form#RemoveTest").serialize(),
                   success:     function(response) {
                                    // Success! Updates the DOM by removing the row in the <table>.
                                    if(response.successful === true) {
                                        DOMelem_radio_btn_selected.parent().parent().remove();
                                    }
                                    
                                    // Error
                                    else {
                                        alert("Error: " + response.error_message);
                                    }
                                }
               });
               
               // Auto-closes the pop-up remove dialog for the user.
               remove_dialog.dialog("close");
           }
            return valid;
        }

        remove_dialog = $("#dialog_form_remove_test").dialog({
            autoOpen: false,
            height: 400,
            width: 350,
            modal: true,
            buttons: {
                "Confirm Removal": removeTest,
                Cancel: function() {
                    remove_dialog.dialog("close");
                }
            },
            close: function() {
                document.getElementById("RemoveTest").reset();
                remove_allFields.removeClass("ui-state-error");
            }
        });

        remove_form = remove_dialog.find("form#dialog_form_remove_test").on("submit", function(event) {
            event.preventDefault();
            removeTest();
        });

        // The "Remove Test" button on the main <form> that causes the pop-up dialog to show.
        $("#remove_test").button().on("click", function() {
            // Error Dialog Message: Cannot remove a test that hasn't been selected.
            if(typeof($("input[type='radio']:checked").val()) === "undefined") {
                // Sets the text of the dialog to say "remove".
                $("#error_edit_or_remove_span").html("remove");
                
                // Error dialog configuration.
                $("#dialog_form_error_message").dialog({
                    resizable: false,
                    height: "auto",
                    width: 400,
                    modal: true,
                    buttons: {
                      Okay: function() {
                        $(this).dialog("close");
                      }
                    }
                });
            }
            
            // No error so far. Shows the pop-up dialog with the data filled in from the record that was selected by the user.
            else {
                // Getters. Grabs the record data of the row selected prior to any modifications by the remove dialog.
                DOMelem_radio_btn_selected = $("input[type='radio']:checked");
                scheduled_sk = DOMelem_radio_btn_selected.val();
                scheduled_subject_id = DOMelem_radio_btn_selected.attr("data-ov-subject-id");
                scheduled_test_condition = DOMelem_radio_btn_selected.attr("data-ov-test-condition");
                scheduled_login_code = DOMelem_radio_btn_selected.attr("data-ov-login-code");
                
                // Setters. Puts the data into the pop-up dialog for the user to remove.
                // Note: The 'sk' field isn't shown, but we need the data to be sent to the server nonetheless.
                $("#remove_test_sk").val(scheduled_sk);
                $("#remove_test_subject_id").val(scheduled_subject_id);
                $("#remove_test_condition").val(scheduled_test_condition);
                $("#remove_login_code").val(scheduled_login_code);
                
                // More setters for the hidden fields as disabled fields cannot submit data to a server per the W3C.
                $("#remove_test_sk_hidden").val(scheduled_sk);
                $("#remove_test_subject_id_hidden").val(scheduled_subject_id);
                $("#remove_test_condition_hidden").val(scheduled_test_condition);
                $("#remove_login_code_hidden").val(scheduled_login_code);
                
                // Opens the pop-up remove dialog for the user.
                remove_dialog.dialog("open");
            }
        });
    });
    
})(window, jQuery);

// @license-end