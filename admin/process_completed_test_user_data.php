<?php
/* --------------------------------------------------------------------------------------------------------------
 * Copyright (C) 2018 by William Paul Liggett (junktext@junktext.com)
 * This Source Code Form is subject to the terms of the Mozilla Public License (MPL), v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Filename:  process_completed_test_user_data.php
 * 
 * Purpose:   Processes a user's activity data after a vigilance test has been completed.
 *            The page is meant to be used as a simple web service, such as with Ajax or the like.
 * 
 * Input:     One JS array with the `ov_random_letter_log' and `ov_user_activity_log' data inside.
 * 
 * Output:    Two separate CSV files named in the manner of:
 *              ov_subject_12_condition_4_on_20180225_2219_raw_1_random_letter_times.csv
 *              ov_subject_12_condition_4_on_20180225_2219_raw_2_user_activity.csv
 * 
 * Details:   The filenames have the "raw" wording because they will be used to create a single, collated CSV
 *            file that attempts to bring together a reliable report. However, since the data is showing when the
 *            user pressed the space bar as the random letters were displayed briefly, there might be false
 *            positives such as if the user pressed the space bar 20 ms into a letter being shown. So, they
 *            simply got it right by accident. Though, it's not clear what determines a false positive so this
 *            is why the design features two raw CSV files that can be used to easily re-generate a single, 
 *            collated CSV report at a later time.
 * -------------------------------------------------------------------------------------------------------------- */

// The file is used in the manner of:
// 1. Outer-Ajax: Log-in user via JS vars
// 2. Inner-Ajax: Submit the Random Letter Log and the User Activity Log

// Enables user sessions.
session_start();

// Function: output_json_response()
require_once "output_json_response.php";

// Function: generate_collated_csv_file_report()
require_once "generate_collated_csv_file_report.php";

// Ensures the user submitting their test data has a valid account.
if(isset($_POST['ov_user_account'])) {
    $ov_user_account = $_POST['ov_user_account'];
    
    // Sanitizes the JS array.
    $ov_user_account[0] = intval($ov_user_account[0]);  // JS: var ov_database_user_sk;
    $ov_user_account[1] = intval($ov_user_account[1]);  // JS: var ov_subject_id;
    $ov_user_account[2] = intval($ov_user_account[2]);  // JS: var ov_test_condition;
    $ov_user_account[3] = htmlspecialchars($ov_user_account[3], ENT_QUOTES, "UTF-8");  // JS: var ov_login_code;
    
    $submitted_username = $ov_user_account[1];
    $submitted_password = $ov_user_account[3];
    
    // Check to see if the PHP session timed out and reopen the session for an authorized user.
    if(!isset($_SESSION['ov_database_user_sk'])) {
        // Logs into the OpenVigilance Task tests database to verify the person is allowed to take a test.
        // `$pdo' is defined as the database connection.
        require_once "../settings.php";
        require_once "../" . PROTECTED_SITE_CONFIGS_DIR . "/openvigilance_db_connection_user.php";
        
        // Confirms whether the login is valid and sets the boolean `$valid_login_user' variable.
        // Also, the user's full account details acquired from the database are stored in `$account_found'.
        require_once "valid_login_user.php";
        
        // Login invalid
        if(!$valid_login_user) {
            output_json_response(false, "User login invalid.");
            return;
        }
        
        // Login valid! Reopens the PHP session.
        $_SESSION['ov_database_user_sk'] = intval($account_found['sk']);
        $_SESSION['ov_user'] = $submitted_username;
        $_SESSION['ov_test_condition'] = intval($account_found['test_condition']);
        $_SESSION['ov_login_code'] = $submitted_password;
        
        // Informs the web browser that the user has logged in successfully.
        output_json_response(true, "");
    }
    
    else {
        // Informs the web browser that the user has logged in successfully.
        output_json_response(true, "");
    }
}

// The user should've been logged in by now. If not, then I won't allow strangers to post strange data.
if(!isset($_SESSION['ov_database_user_sk'])) {
    output_json_response(false, "The user must be logged in before submitting test data.");
    return;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++        Authenticated user! We'll go ahead and attempt to create the necessary CSV files on the server.       ++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// -----------------------------------------------------------------------------------------------------------------------
// Random Letter Log: Details when each random letter was presented. No user activity is recorded.
// -----------------------------------------------------------------------------------------------------------------------
if(isset($_POST['ov_raw_input_data'])) {
    // JS string. Unfortunately, this is not a direct JS array as originally built by the client.
    // The reason for this is that there is so much data created with the random letters that submitting it
    // directly as a JS array for standard processing has the side-effect of breaching PHP's `max_input_var' setting.
    // For instance with max_input_vars=1000 the `ov_random_letter_log' gets cut off on key 166 and, likewise, with
    // max_input_var=5000, the `ov_random_letter_log' gets cut off at key 833 of 1531 (from a full 12+12=24 min OV test).
    // Note: The $_POST string is sanitized later.
    $string_ov_random_letter_log = $_POST['ov_raw_input_data'][0];
    
    // Breaks the string into a single-dimension array.
    $flat_array_ov_random_letter_log = explode(",", $string_ov_random_letter_log);
    
    // Builds up a two-dimensional array like it was stored in JS.
    $ov_random_letter_log = array();
    $temp_array = array();
    $column = 0;
    for($i = 0; $i < count($flat_array_ov_random_letter_log); $i++) {
        array_push($temp_array, $flat_array_ov_random_letter_log[$i]);
        if($column < 5) {
            $column++;
        }
        else {
            array_push($ov_random_letter_log, $temp_array);
            $temp_array = array();
            $column = 0;
        }
    }

    // Sanitizes the JS array.
    foreach($ov_random_letter_log as $key => &$value) {
        // Title heading should only have: "GlobalTrialCounter", "CurrentTrial", "TimeStamp", "Type", "Block", 
        // and "ov_random_letter_is_o".
        if($key === 0) {
            if(
                ($value[0] !== "GlobalTrialCounter") && 
                ($value[1] !== "CurrentTrial") &&
                ($value[2] !== "TimeStamp") &&
                ($value[3] !== "Type") &&
                ($value[4] !== "Block") &&
                ($value[5] !== "ov_random_letter_is_o") 
            ) {
                output_json_response(false, "Bad title headings for ov_random_letter_log at key: " . intval($key) . 
                                     ", value (full row): " . var_export($value, true));
                return;
            } 

            // The title headings were validated. Skip to the next row in the array.
            else {
                continue;
            }
        }

        // Safely converts any data submitted to only interval numbers.
        $value[0] = intval($value[0]);  // GlobalTrialCounter
        $value[1] = intval($value[1]);  // CurrentTrial
        $value[2] = intval($value[2]);  // TimeStamp in Unix time (ms)
        
        // Used for naming both of the raw CSV data files.
        if($key === 1) {
            // This is the datetime in ms since 1970, from when the 1st random letter was shown in the practice test.
            $datetime_since_1970 = $value[2];
        }
        
        // Type: Must be either "P" or "E".
        $value[3] = htmlspecialchars($value[3], ENT_QUOTES);
        if(($value[3] !== "P") && ($value[3] !== "E")) {
            output_json_response(false, "Invalid 'Type' data for ov_random_letter_log at key: " . intval($key) . 
                                 ", value (full row): " . var_export($value, true));
            return;
        }
        
        $value[4] = intval($value[4]);  // Block
        
        // ov_random_letter_is_o: Must be either 0 or 1.
        $value[5] = intval($value[5]);
        if(($value[5] !== 0) && ($value[5] !== 1)) {
            output_json_response(false, "Invalid 'ov_random_letter_is_o' data for ov_random_letter_log at key: " . 
                                 intval($key) . ", value (full row): " . var_export($value, true));
            return;
        }
    }

    // Recommended by the PHP Manual
    unset($value);
    
    // Sets up the filename to be used for the CSV.
    date_default_timezone_set("America/New_York");
    
    // Date/Time Format: YYYYMMDD_HHMM with leading zeroes (e.g., 20180302_0908)
    $datetime = date("Ymd_Hi", substr($datetime_since_1970, 0, 10));
    $tz_abbr = date("T");  // Abbreviated timezone, such as: EST
    
    // File path:
    $filepath = "../user-data-files/raw-input/";
    
    // Filename format example: ov_subject_12_condition_4_on_20180225_2219_EST_raw_1_random_letter_times.csv
    $filename_raw_1 = "ov_subject_" . strval($_SESSION['ov_user']) . "_condition_" . strval($_SESSION['ov_test_condition']) .
                      "_on_" . $datetime . "_" . $tz_abbr . "_raw_1_random_letter_times.csv";
    
    // Creates the CSV file and stores it on the web server.
    try {
        $new_csv_file = @fopen($filepath . $filename_raw_1, "w");
        
        // Checks whether the file pointer is valid.
        if(!$new_csv_file) {
            throw new Exception();
        }

        foreach($ov_random_letter_log as $csv_data) {
            fputcsv($new_csv_file, $csv_data);
        }

        fclose($new_csv_file);

        // Sets the file to be read-only to indicate that no data should ever be modified directly later on!
        chmod($filepath . $filename_raw_1, 0444);
    }
    
    // Something went wrong with the new CSV file.
    catch (\Exception $e) {
        output_json_response(false, "Exception: Cannot create a new CSV file for some reason.");
        return;
    }

    // -----------------------------------------------------------------------------------------------------------------------
    // User Activity Log: Documents when the user pressed the space bar.
    // -----------------------------------------------------------------------------------------------------------------------
    // JS string. Unfortunately, this is not a direct JS array as originally built by the client.
    // The reason for this is that there is so much data created with the user that submitting it
    // directly as a JS array for standard processing has the side-effect of breaching PHP's `max_input_var' setting.
    // Note: The $_POST string is sanitized later.
    $string_ov_user_activity_log = $_POST['ov_raw_input_data'][1];
    
    // Breaks the string into a single-dimension array.
    $flat_array_ov_user_activity_log = explode(",", $string_ov_user_activity_log);
    
    // Builds up a two-dimensional array like it was stored in JS.
    $ov_user_activity_log = array();
    $temp_array = array();
    $column = 0;
    for($i = 0; $i < count($flat_array_ov_user_activity_log); $i++) {
        array_push($temp_array, $flat_array_ov_user_activity_log[$i]);
        if($column < 1) {
            $column++;
        }
        else {
            array_push($ov_user_activity_log, $temp_array);
            $temp_array = array();
            $column = 0;
        }
    }

    // Sanitizes the JS array.
    foreach($ov_user_activity_log as $key => &$value) {
        // Title heading should only have: "GlobalTrialCounter" and "ov_participant_response_time".
        if($key === 0) {
            if(($value[0] !== "GlobalTrialCounter") && ($value[1] !== "ov_participant_response_time")) {
                output_json_response(false, "Bad title headings for ov_user_activity_log at key: " . intval($key) . 
                                     ", value (full row): " . var_export($value, true));
                return;
            } 

            // The title headings were validated. Skip to the next row in the array.
            else {
                continue;
            }
        }

        // Safely converts any data submitted to only interval numbers.
        $value[0] = intval($value[0]);  // GlobalTrialCounter
        $value[1] = intval($value[1]);  // ov_participant_response_time in Unix time (ms)
    }

    // Recommended by the PHP Manual
    unset($value);
    
    // Filename format example: ov_subject_12_condition_4_on_20180225_2219_EST_raw_2_user_activity.csv
    // The `$datetime' and `$tz_abbr' was set previously when processing the 1st raw file (random letter times).
    $filename_raw_2 = "ov_subject_" . strval($_SESSION['ov_user']) . "_condition_" . strval($_SESSION['ov_test_condition']) .
                      "_on_" . $datetime . "_" . $tz_abbr . "_raw_2_user_activity.csv";
    
    // Creates the CSV file and stores it on the web server.
    try {
        $new_csv_file = @fopen($filepath . $filename_raw_2, "w");
        
        // Checks whether the file pointer is valid.
        if(!$new_csv_file) {
            throw new Exception();
        }

        foreach($ov_user_activity_log as $csv_data) {
            fputcsv($new_csv_file, $csv_data);
        }

        fclose($new_csv_file);

        // Sets the file to be read-only to indicate that no data should ever be modified directly later on!
        chmod($filepath . $filename_raw_2, 0444);
    }
    
    // Something went wrong with the new CSV file.
    catch (\Exception $e) {
        output_json_response(false, "Exception: Cannot create a new CSV file for some reason.");
        return;
    }

    $collated_csv_creation_msg = generate_collated_csv_file_report($filename_raw_1, $filename_raw_2);
    
    if($collated_csv_creation_msg === true) {
        // Reports that all of the CSV files were created successfully. Raw 1, Raw 2, and the Collated CSV.
        output_json_response(true, "");
        return;
    }
    
    else {
        // There was some problem creating all of the CSV files, especially with the collated CSV.
        output_json_response(false, $collated_csv_creation_msg);
        return;
    }
}
