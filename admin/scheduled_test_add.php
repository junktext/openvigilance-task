<?php
/* --------------------------------------------------------------------------------------------------------------
 * Copyright (C) 2018 by William Paul Liggett (junktext@junktext.com)
 * This Source Code Form is subject to the terms of the Mozilla Public License (MPL), v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Filename:  scheduled_test_add.php
 * 
 * Purpose:   Add a new OpenVigilance Task test in the database.
 *            The page is meant to be used as a simple web service, such as with Ajax or the like.
 * 
 * Output:    JSON array as defined in ``output_json_response.php''.
 * -------------------------------------------------------------------------------------------------------------- */

// Maintains the authenticated user session across different admin pages.
// Ensures only valid administrators can do anything.
session_start();

// Function: output_json_response($successful, $error_message)
require_once "output_json_response.php";

if(!isset($_SESSION['ov_admin_user'])) {
    output_json_response(false, "Not logged on as an administrator.");
    return;
}

// -------------------------------------------------------------------------------
// POST input variables sent by the web browser.
// Note: The login_code cannot be set by an OV admin. The code is auto-generated.
// -------------------------------------------------------------------------------
//$sk = intval($_POST['add_test_sk']);  // Surrogate Key identifier of the scheduled test in the database.
$subject_id = intval($_POST['add_test_subject_id']);  // The ID of the person meant to take the test.
$test_condition = intval($_POST['add_test_condition']);  // The kind of test being taken.

// Validate the data.
if($subject_id <= 0 || $test_condition <= 0) {
    output_json_response(false, "The subject_id nor the test_condition cannot be less than or equal to zero.");
    return;
}

else if($test_condition > 4) {
    output_json_response(false, "The test_condition cannot be greater than 4.");
    return;
}

// Logs into the OpenVigilance Task tests database to control and alter user tests.
// `$pdo' is defined as the database connection.
require_once "../settings.php";
require_once "../" . PROTECTED_SITE_CONFIGS_DIR . "openvigilance_db_connection_admin.php";

// Auto-generate the subject's (user's) login code.
$login_code = "";
while(strlen($login_code) !== 4) {
    $rand_num = rand(48, 122);  // ASCII: Between numbers and the letter 'z'.
    
    // Only create a login code that matches the style of numbers or lower-case letters, like: 7a2b
    // So, we don't want these ASCII codes. (Punctuation and Upper-case letters.)
    if($rand_num >= 58 and $rand_num <= 96) {
        continue;
    }
    
    else {
        // Appends the ASCII char to the `$login_code' string.
        $login_code .= chr($rand_num);
    }
}

// Test Control: Add the OV test to the database.
$sql = "INSERT INTO test_control (subject_id, test_condition, login_code) VALUES (:subject_id, :test_condition, :login_code)";
$statement = $pdo->prepare($sql);
$statement->bindValue(":subject_id", $subject_id, PDO::PARAM_INT);
$statement->bindValue(":test_condition", $test_condition, PDO::PARAM_INT);
$statement->bindValue(":login_code", $login_code, PDO::PARAM_STR);
$successful_insert = $statement->execute();  // true or false

if($successful_insert) {
    // Finds the new surrogate key (sk) from the recently inserted row.
    $sql = "SELECT sk FROM test_control WHERE subject_id=:subject_id AND test_condition=:test_condition AND login_code=:login_code";
    $statement = $pdo->prepare($sql);
    $statement->bindValue(":subject_id", $subject_id, PDO::PARAM_INT);
    $statement->bindValue(":test_condition", $test_condition, PDO::PARAM_INT);
    $statement->bindValue(":login_code", $login_code, PDO::PARAM_STR);
    $statement->execute();
    $new_sk = $statement->fetch()[0];
    
    // Inform the web browser that all went well and informs the Ajax client what the login_code is for the subject.
    output_json_response(true, "", strval($login_code), strval($new_sk));
    return;
}

else {
    // The database was not properly updated for some reason.
    output_json_response(false, "The input data was valid, but the database could not be updated at this time for some reason.");
    return;
}
