<?php
    // Enables user sessions.
    session_start();
    
    // Force the user to connect via "https://".
    if($_SERVER["HTTPS"] != "on") {
        header("Location: https://" . $_SERVER["HTTP_HOST"] . htmlspecialchars($_SERVER["REQUEST_URI"], ENT_QUOTES, "UTF-8"));
        exit();
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>OpenVigilance Task</title>
        <!-- Copyright (C) 2018 by William Paul Liggett (junktext@junktext.com)
             This Source Code Form is subject to the terms of the Mozilla Public License (MPL), v. 2.0.
             If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
        -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Sustained attention tasks using random letters of O, D, and backwards D to help determine what influences a person's reaction time." />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
    </head>
    <body>
        <?php
            error_reporting(E_ALL);
            
            // Some pages don't need this <h1> title tag, but for those that do we'll define it once to be DRY.
            $h1_page_title = "<h1 id=\"h1_page_title\">OpenVigilance Task</h1>" . PHP_EOL;
        
            // Makes the tab whitespace makes the HTML source look nice. There are 4 spaces.
            $tab = "    ";
        
            // Displays if any login errors were detected. No error = Non-breaking space character. 
            // HTML needs something besides an empty string to display the element to the DOM.
            $error_msg = "&nbsp;";
        
            // Used to display an HTML login form on first load or if the username and password was incorrect.
            // The <form action="." just simply means it sends the request to this page (index.php) in the current directory.
            $login_form = PHP_EOL .
                          "$tab$tab<form action=\".\" name=\"UserLogin\" id=\"UserLogin\" method=\"POST\">" . PHP_EOL .
                          "$tab$tab$tab<label for=\"username\">Username:</label>" . PHP_EOL .
                          "$tab$tab$tab<input type=\"text\" name=\"username\" id=\"username\" />" . PHP_EOL .
                          "$tab$tab$tab<br />" . PHP_EOL .
                          "$tab$tab$tab<label for=\"password\">Password:</label>" . PHP_EOL .
                          "$tab$tab$tab<input type=\"password\" name=\"password\" id=\"password\" />" . PHP_EOL .
                          "$tab$tab$tab<br />" . PHP_EOL .
                          "$tab$tab$tab<input type=\"submit\" value=\"Submit\" />" . PHP_EOL .
                          "$tab$tab</form>" . PHP_EOL;
            
            // GET: The page upon first load before the <form> has been submitted, or when a user clicks the "Sign Out" button
            // once they've taken an OpenVigilance Task test.
            if($_SERVER['REQUEST_METHOD'] === "GET") {
                // Displays the HTML login <form>.
                // There should be no error on first load, but this keeps the form layout in the same position.
                echo $h1_page_title;
                
                // MAINTENANCE MODE: Uncomment the line below and comment out the NO MAINTENANCE lines to disable logins.
                //echo "<p class=\"center\">Sorry, the system is under maintenance. Please come back later.</p>" . PHP_EOL;
                
                // NO MAINTENANCE (Default) -- Uncomment the two lines to enable a participant's login capability.
                echo "<p class='error_msg'>$error_msg</p>";
                echo $login_form;
            }
        
            // POST: Confirms if the administrative password is accurate after the <form> has been submitted.
            // Note: I am specifically defining an `else if' clause since REQUEST_METHOD could be: GET, POST, HEAD, or PUT
            else if($_SERVER['REQUEST_METHOD'] === "POST") {
                // Closes out the user's previous session. This can happen since the OpenVigilance Task app might be used by 
                // multiple people using the same computer (e.g., person A starts a test, while person B waits for A to finish). 
                // So, we'll just disconnect any old session data in order to avoid the glitch where person B can sign-in with 
                // their login information but they accidentally inherit person A's old session variables.
                if(isset($_SESSION['ov_database_user_sk'])) {
                    $_SESSION = array();
                    session_regenerate_id();
                }
                
                // Grabs what the user submitted and sanitizes the input to avoid an SQL injection or XSS attack.
                $submitted_username = intval($_POST['username']);  // Regular users only have numeric account names.
                $submitted_password = htmlspecialchars($_POST['password'], ENT_QUOTES, 'UTF-8');
                
                // Logs into the OpenVigilance Task tests database to verify the person is allowed to take a test.
                // `$pdo' is defined as the database connection.
                require_once "settings.php";
                require_once PROTECTED_SITE_CONFIGS_DIR . "openvigilance_db_connection_user.php";
                
                // Confirms whether the login is valid and sets the boolean `$valid_login_user' variable.
                // Also, the user's full account details acquired from the database are stored in `$account_found'.
                require_once "admin/valid_login_user.php";
                
                // Login invalid: Re-displays the login <form>.
                if(!$valid_login_user) {
                    echo $h1_page_title;
                    echo "<p class='error_msg'>$error_msg</p>";
                    echo $login_form;
                }
             
                // Login successful!
                else {
                    // Maintains the authenticated user session across different pages.
                    // The surrogate key in the database must be unique, so there's no need to check
                    // the other session variables even further initially.
                    if(!isset($_SESSION['ov_database_user_sk'])) {
                        $_SESSION['ov_database_user_sk'] = intval($account_found['sk']);
                        $_SESSION['ov_user'] = $submitted_username;
                        $_SESSION['ov_test_condition'] = intval($account_found['test_condition']);
                        $_SESSION['ov_login_code'] = $submitted_password;
                    }
                    
                    // -----------------------------------------------------------------------------------------------------------
                    // Changes up what the test participant sees and whether they get a break and what kind of break they'll have.
                    // 
                    // Test Condition #1: No break. Two back-to-back 12 minute task blocks.
                    // Test Condition #2: 12 minute task. Break w/a blank, white screen. 12 minute task again.
                    // Test Condition #3: 12 minute task. Break w/a nature video. 12 minute task again.
                    // Test Condition #4: 12 minute task. Break w/random letters still flashing. 12 minute task again.
                    // -----------------------------------------------------------------------------------------------------------
                    
                    $login_welcome_msg_no_break = "<div id=\"login_welcome_msg\"><p>" . PHP_EOL .
                        "In this experiment, letters will appear rapidly in the center of the screen. They will appear behind a "  . PHP_EOL .
                        "large pattern that is made up of small circles. The letters will either be an O, D, or a backwards D. "   . PHP_EOL .
                        "Your task is to detect the letter O. <strong>Each time that the letter O appears, press the space bar."   . PHP_EOL .
                        "</strong> You will have approximately 1 second to respond before the next letter appears so respond as "  . PHP_EOL .
                        "rapidly as you can while being accurate.</p>" . PHP_EOL .
                            
                        "<p>When either the letter D or backwards D appears, you should not make any response. There will be a "   . PHP_EOL .
                        "short practice block before the task begins. After the practice block, the task will last approximately " . PHP_EOL .
                        "25 minutes.</p>" . PHP_EOL .

                        "<p>If you have any questions please ask the experimenter now. When you are ready, press the \"Start "     . PHP_EOL .
                        "Test\" button to continue on to the practice block.</p></div>" . PHP_EOL;
                    
                    $login_welcome_msg_with_break = "<div id=\"login_welcome_msg\"><p>" . PHP_EOL .
                        "In this experiment, letters will appear rapidly in the center of the screen. They will appear behind a "  . PHP_EOL .
                        "large pattern that is made up of small circles. The letters will either be an O, D, or a backwards D. "   . PHP_EOL .
                        "Your task is to detect the letter O. <strong>Each time that the letter O appears, press the space bar."   . PHP_EOL .
                        "</strong> You will have approximately 1 second to respond before the next letter appears so respond as "  . PHP_EOL .
                        "rapidly as you can while being accurate.</p>" . PHP_EOL .
                            
                        "<p>When either the letter D or backwards D appears, you should not make any response. There will be a "   . PHP_EOL .
                        "short practice block before the task begins. After the practice block, the task will last approximately " . PHP_EOL .
                        "25 minutes. You will have a short break at the midpoint of the task. During this break please remain in " . PHP_EOL .
                        "your seat, and do not take out your phone or any other personal items.</p>" . PHP_EOL .

                        "<p>If you have any questions please ask the experimenter now. When you are ready, press the \"Start "     . PHP_EOL .
                        "Test\" button to continue on to the practice block.</p></div>" . PHP_EOL;

                    $start_test_button = "<input type=\"button\" name=\"StartTestButton\" id=\"StartTestButton\" value=\"Start Test\" />" . PHP_EOL;
                    
                    // The JavaScript variables properly select the correct test and also log the user's activity appropriately.
                    // These may seem redundant with the PHP $_SESSION[] variables, but because the default php.ini config
                    // times out session information after 24 minutes, this would require every OpenVigilance Task program to be
                    // properly extended by each person who might host the web app. This aspect is compounded by the fact that
                    // a standard vigilance task with a 4-minute break will last at least 29 minutes from:
                    // 30 second practice + 30 second break + 12 minute test + 4 minute break + 12 minute test
                    // 
                    // Furthermore, if a user for some reason logs into the system but does NOT click the "Start Test" button, as
                    // they might need to step away for some reason, then the PHP session variables can time out before the user
                    // even begins the test. With all of this in mind, embedding these variables in JavaScript for the client 
                    // helps us to reconfirm that any user data submitted at the end of the test was really from an authorized 
                    // user who might have had the PHP session variables time out for some reason. So, overall, this keeps the
                    // program to be more resilient in general.
                    $js_var_database_user_sk = "var ov_database_user_sk = parseInt(" . $_SESSION['ov_database_user_sk'] . ");" . PHP_EOL;
                    $js_var_subject_id =     "var ov_subject_id = parseInt(" . $_SESSION['ov_user'] . ");" . PHP_EOL;
                    $js_var_test_condition = "var ov_test_condition = parseInt(" . $_SESSION['ov_test_condition'] . ");" . PHP_EOL;
                    $js_var_login_code = "var ov_login_code = \"" . $_SESSION['ov_login_code'] . "\";" . PHP_EOL;
                    
                    // ---------------------------------------------------------------
                    // Test Condition #1: No break. See above description for details.
                    // ---------------------------------------------------------------
                    if($_SESSION['ov_test_condition'] === 1) {
                        $login_welcome_msg = $login_welcome_msg_no_break;
                    }
                    
                    // ----------------------------------------------------------------------------
                    // Test Conditions #2-4: With a 4-min break. See above description for details.
                    // ----------------------------------------------------------------------------
                    else {
                        $login_welcome_msg = $login_welcome_msg_with_break;
                    }
                    
                    // ------------------------------------------------
                    // Outputs the correct HTML for any test condition.
                    // ------------------------------------------------
                    echo "<script>" . PHP_EOL;
                    echo $tab . $tab . $tab . $js_var_database_user_sk;
                    echo $tab . $tab . $tab . $js_var_subject_id;
                    echo $tab . $tab . $tab . $js_var_test_condition;
                    echo $tab . $tab . $tab . $js_var_login_code;
                    echo $tab . $tab . "</script>" . PHP_EOL;
                    echo $tab . $tab . $h1_page_title;
                    echo $tab . $tab . $login_welcome_msg;
                    echo $start_test_button;
                    
                    // Displays the OV test (images and so forth) to the user. JavaScript is used to unhide it.
                    echo "<span id=\"vigilance_display_banner_text\">&nbsp;</span>" . PHP_EOL .
                         "$tab$tab$tab<span id=\"vigilance_display_extra_text\">&nbsp;</span>" . PHP_EOL .
                         "$tab$tab$tab<div class=\"container hidden\" id=\"VigilanceDisplay\">" . PHP_EOL .
                         "$tab$tab$tab<img id=\"background-smaller-circles\" src=\"media/background-smaller-circles_(936_by_466_px,_Resolution_at_1360x768_~16_by_9).png\" alt=\"Background of Smaller Circles\" />" . PHP_EOL .
                         "$tab$tab$tab<div class=\"random_letter\">" . PHP_EOL .
                         "$tab$tab$tab$tab<span class=\"letter_o\">O</span>" . PHP_EOL .
                         "$tab$tab$tab$tab<span class=\"letter_d\">D</span>" . PHP_EOL .
                         "$tab$tab$tab$tab<span class=\"letter_backwards_d\">D</span>" . PHP_EOL .
                         "$tab$tab$tab</div>" . PHP_EOL .
                         "$tab$tab</div>" . PHP_EOL;
                    
                    // This special if-else block is here because this web app is unique in that it needs the media to be
                    // present immediately with no download delays. Also, doing this ensures that if a user gets disconnected,
                    // they can still have the same experience as fully online test takers.
                    // 
                    // Test Condition #3 needs the nature video to be downloaded and ready to play.
                    if($_SESSION['ov_test_condition'] === 3) {
                        echo "<div id=\"nature_video_div\" class=\"hidden\">" . PHP_EOL .
                             "$tab$tab<video id=\"nature_video\" width=\"640\" height=\"360\">" . PHP_EOL .
                             // The file "media/OpenVigilance_Nature_Video.mp4" will be added to this <div>'s src via JS later.
                             "$tab$tab</video>" . PHP_EOL .
                             "</div>" . PHP_EOL;
                    }
                    
                    // Test Condtion #4 needs the extra `break_reminder_for_condition_4.png' so it can pop-up when needed.
                    else if($_SESSION['ov_test_condition'] === 4) {
                        echo "<div id=\"break_reminder_popup\" class=\"hidden\">" . PHP_EOL .
                             // The <img> tag will be added here via JavaScript later.
                             "</div>" . PHP_EOL;
                    }
                    
                    // Sets up a hidden form to be used to sign-out a user after they've finished taking a test.
                    echo "$tab$tab<div id=\"user_sign_out\" class=\"hidden\">" . PHP_EOL .
                         "$tab$tab$tab<form action=\".\" name=\"UserSignOut\" id=\"UserSignOut\" method=\"GET\">" . PHP_EOL .
                         "$tab$tab$tab$tab<input id=\"user_sign_out_button\" type=\"submit\" value=\"Sign Out\" />" . PHP_EOL .
                         "$tab$tab$tab</form>" . PHP_EOL .
                         "$tab$tab</div>" . PHP_EOL;
                }
            }
        ?>
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/openvigilance-task.js"></script>
    </body>
</html>
